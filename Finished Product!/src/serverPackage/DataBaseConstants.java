package serverPackage;
/**
 * Interface holding constants for Database
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @since April 3rd, 2018
 * @version 1.0
 */
public interface DataBaseConstants {
	/**
	 * Database URL
	 */
	public String DB_URL = "jdbc:mysql://localhost:3306";
	/**
	 * Database name
	 */
	public static String DB_NAME = "CourseManagementSystem_DB";
	/**
	 * User name for Database
	 */
	public String USER_NAME = "username";
	/**
	 * Password for database
	 */
	public String PASSWORD = "password";
	/**
	 * Name of table of assignments in database
	 */
	public String ASSIGNMENT_TABLE = "Assignment_Table";
	/**
	 * Name of table of Student Enrollments  in database
	 */
	public String STUDENT_ENROLL_TABLE = "Enrollment_Table";
	/**
	 * Name of table of submissions in database
	 */
	public String SUBMISSION_TABLE = "Submission_Table";
	/**
	 * Name of table of courses in database
	 */
	public String COURSE_TABLE= "Course_Table";
	/**
	 * Name of table of users in database
	 */
	public String USER_TABLE= "User_Table";
}
