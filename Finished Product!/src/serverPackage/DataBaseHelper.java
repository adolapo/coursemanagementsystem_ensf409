package serverPackage;

import data.*;

import java.io.File;
import java.sql.Connection;
import java.sql.Driver;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import javax.print.attribute.standard.PrinterLocation;
/**
 * Program that manages the SQL Database holding information about students and professors,
 * their courses, assignments, sumissions, and grades
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @version 1.0
 * @since April 4, 2018
 */
class DataBaseHelper implements DataBaseConstants{
	//TODO -- Implement path builder function to enable path storage in SQL (account for "\")
	/**
	 * The Connection object to the database
	 */
	private Connection connection;
	/**
	 * The Statement object used for executing SQL statements
	 */
	private Statement statement;
	/**
	 * The FileHelper used in handling file strorage and retrieval
	 */
	private FileHelper fileWorker;
	
	/**
	 * Constructs a database helper that will create the data base depending on parameters
	 * @param createDB determines whether a database is created
	 */
	public DataBaseHelper(boolean createDB) {
		try {
			fileWorker = new FileHelper();
			
			Driver driver = new com.mysql.jdbc.Driver();
			DriverManager.registerDriver(driver);
			
			//Get Connection
			connection = DriverManager.getConnection(DB_URL, USER_NAME, PASSWORD);
		}
		catch(SQLException e) { 
			System.err.println("COULD NOT CONNECT TO DATABASE. Make sure url, username, and"
								+ "password are correct");
			e.printStackTrace(); 
		}
		
		if(createDB) { //Database and tables need be made only once
			//createDatabase();
			//createTables();
		}
		
	}
	
	
	/**
	 * Creates Database
	 */
	public void createDatabase() {
		String query ="CREATE DATABASE " + DB_NAME;
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Successfully created Database");
		}
		catch(SQLException e){
			System.err.println("SQL ERROR: Could not create Database");
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates all tables required by the database
	 */
	private void createTables()
	{
		createUserTable();
		createCourseTable();
		createStudentEnrollmentTable();
		createAssignmentTable();
		createSubmissionTable();
	}
	
	//-------------------------------------------------------------------------------------------------------------
	//------------------------FUNCTIONS TO CREATE DATABASE TABLES--------------------------------------------------
	//-------------------------------------------------------------------------------------------------------------
	
	/**
	 * Creates User table
	 */
	private void createUserTable() {
		try {
			String query = "CREATE TABLE " +
					DB_NAME + "." + USER_TABLE +
					"(" +
					"ID INT(8) NOT NULL, " +
					"PASSWORD VARCHAR(20) NOT NULL, " +
					"EMAIL VARCHAR(50) NOT NULL, " +
					"FIRST_NAME VARCHAR(30) NOT NULL, " +
					"LAST_NAME VARCHAR(30) NOT NULL, " +
					"TYPE CHAR(1) NOT NULL, " +
					"PRIMARY KEY ( id )" +
					")";
			statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Successfully created user table");
		}
		catch(SQLException e) {
			System.err.println("ERROR IN CREATING USER TABLE");
			e.printStackTrace();
		}
	}
	
	
	/** 
	 * Creates Course table
	 */
	public void createCourseTable() {
		try {
			String query = "CREATE TABLE " +
							DB_NAME + "." + COURSE_TABLE +
							"(" +
							"ID INT(8) NOT NULL, " +
							"PROF_ID INT(8) NOT NULL, " +
							"NAME VARCHAR(50) NOT NULL, " +
							"ACTIVE INT(1) NOT NULL, " +
							"PRIMARY KEY (id)" +
							")";
			statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Successfully created Course Table");
		}
		catch(SQLException e) {
			System.err.println("ERROR IN CREATING COURSE TABLE");
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates Student Enrollment table
	 */
	public void createStudentEnrollmentTable() {
		try {
			String query = "CREATE TABLE " +
							DB_NAME + "." + STUDENT_ENROLL_TABLE +
							"(" +
							"ID INT(8) NOT NULL, " +
							"STUDENT_ID INT(8) NOT NULL, " +
							"COURSE_ID INT(8) NOT NULL, " +
							"PRIMARY KEY ( id )" +
							")";
			statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Successfully created Student enrollment table");
			
		}
		catch(SQLException e) {
			System.err.println("ERROR IN CREATING STUDENT ENROLLMENT TABLE");
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates Assignment table
	 */
	private void createAssignmentTable(){
		try {
			String query = "CREATE TABLE " + 
							DB_NAME + "." + ASSIGNMENT_TABLE +
							"(" +
							"ID INT(8) NOT NULL, " + 
							"COURSE_ID INT(8) NOT NULL, " +
							"TITLE VARCHAR(50) NOT NULL, " +
							"PATH VARCHAR(300) NOT NULL, " +
							"ACTIVE INT(1) NOT NULL, " +
							"DUE_DATE VARCHAR(16) NOT NULL, " +
							"PRIMARY KEY ( id )" +
							")";
							
			statement = connection.createStatement();
			statement.executeUpdate(query);	
			System.out.println("Successfully created Assignment table");
		}
		catch(SQLException e) {
			System.err.println("ERROR IN CREATING ASSIGNMENT TABLE");
			e.printStackTrace();
		}
	}
	
	/**
	 * Creates submission table
	 */
	public void createSubmissionTable() {
		String query = "CREATE TABLE " +
						DB_NAME + "." + SUBMISSION_TABLE +
						"(" +
						"ID INT(8) NOT NULL, " +
						"ASSIGN_ID INT(8) NOT NULL, " +
						"STUDENT_ID INT(8) NOT NULL, " +
						"PATH VARCHAR(300) NOT NULL, " +
						"TITLE VARCHAR(50) NOT NULL, " +
						"COMMENTS VARCHAR(140) NOT NULL, " +
						"TIME_STAMP CHAR(30) NOT NULL, " +
						"GRADE INT(3) NOT NULL, " +
						"PRIMARY KEY ( id )" +
						")";
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			System.out.println("Successfully created Submission table");
		}
		catch(SQLException e) {
			System.err.println("ERROR CREATING SUBMISSION TABLE");
			e.printStackTrace();
		}
	}
	
	//-----------------------------------------------------------------------------------------------------
	//------------------------------FUNCTIONS TO SEARCH AND BUILD OBJECTS FROM DATABASE--------------------
	//-----------------------------------------------------------------------------------------------------
	
	/**
	 * Searches User table by User Id to get the email address of the user
	 * @param userId ID of user
	 * @return user's email address if it exists, else null
	 */
	public String getEmailAddress(int userId) {
		String sender = null;
		String query = "SELECT * FROM " + DB_NAME + "." + USER_TABLE +
				" WHERE ID = " + userId;
		try {
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);
			if(result.next()) {
				sender = result.getString("EMAIL");
			}	
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE SEARCHING USER TABLE FOR EMAIL");
			e.printStackTrace();
		}
		return sender;
	}
	
	/**
	 * Gets the email addresses of all students enrolled in a course
	 * @param courseID the ID of the course 
	 * @return a list of email addresses
	 */
	public ArrayList<String> getStudentsAddress(int courseID){
		ArrayList<String> receivers = new ArrayList<>();
		Message m = searchStudentEnrollmentTableByCourseId(courseID);
		ArrayList<Enrollment> enrollmentList= (ArrayList<Enrollment>)m.getData();
		for(Enrollment e : enrollmentList) {
			String address = getEmailAddress(e.getStudentID());
			if(address == null)
				System.err.println("ERROR WHEN GETTING EMAIL ADDRESS");
			else
				receivers.add(address);
		}
		return receivers;
	}
	
	/**
	 * Searches User table by User Id
	 * @param userId ID of user to be returned
	 * @return a valid Message objectcontaining the found User object or null if no user found
	 */
	public Message searchUserTable(int userId) {
		Message response = null;
		User foundUser = null;
		
		String query = "SELECT * FROM " + DB_NAME + "." + USER_TABLE +
						" WHERE ID = " + userId;
		try {
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);
			if(result.next()) {
				System.out.println("FOUND USER");
				 foundUser = new User(result.getString("TYPE"),
										  result.getString("FIRST_NAME"),
										  result.getString("LAST_NAME"),
										  result.getInt("ID"));
				response = new Message(0, foundUser);
				return response;
			}
			
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE SEARCHING ASSIGNMENT TABLE");
			e.printStackTrace();
		}
		return new Message(0, null); //no user found
	}
	
	/**
	 * Verifies user log in
	 * @param login Login object containing login details
	 * @return User if user exists and passwords match, a null Message object otherwise
	 */
	public Message verifyLogin(Login login) {
		String query = "SELECT PASSWORD FROM " + DB_NAME + "." + USER_TABLE +
					   " WHERE ID = " + login.getLoginId();
		try {
			statement = connection.createStatement();
			ResultSet results = statement.executeQuery(query);
			if(results.next()) {
				if(login.getPass().equals(results.getString("PASSWORD")))
					return searchUserTable(login.getLoginId());
			}		
		}
		catch(SQLException e) {
			System.err.println("ERROR IN LOGIN");
			e.printStackTrace();
		}
		return new Message(0, null);
	}
	
	/**
	 * Constructs a StudentUser object for a certain userID 
	 * @param userId the userID
	 * @return Message contains constructed StudentUser object that matches the userID
	 */
	public Message constructStudentUser(int userId) {
		StudentUser foundUser = null;
		String query = "SELECT * FROM " + DB_NAME + "." + USER_TABLE +
					   " WHERE ID = " + userId;
		try {
			statement = connection.createStatement();
			ResultSet result = statement.executeQuery(query);
			if(result.next()) {
				//Search for course IDs for courses student is taking
				ArrayList<Integer> listCourseIds = new ArrayList<Integer>();
				String query2 = "SELECT COURSE_ID FROM " + DB_NAME + "." + STUDENT_ENROLL_TABLE +
								" WHERE STUDENT_ID = " + userId;
				try {
					Statement statement2 = connection.createStatement();
					ResultSet resultCourseIds = statement2.executeQuery(query2);
					while(resultCourseIds.next()) {
						listCourseIds.add(resultCourseIds.getInt("COURSE_ID"));
					}
				}
				catch(SQLException e) {
					System.err.println("ERROR WHILE SEARCHING ENROLLMENT TABLE FOR COURSE IDS WITHIN findStudentUser");
					e.printStackTrace();
				}
				//Use found IDs to search for course objects
				ArrayList<Course> listCourses = new ArrayList<Course>();
				for(int i = 0; i < listCourseIds.size(); i++) {//Find Student's courses
					Message temp = constructCourseByCourseID(listCourseIds.get(i));
					listCourses.add((Course)temp.getData());
				}
				//Construct Student User
				foundUser = new StudentUser(result.getString("TYPE"),
										  result.getString("FIRST_NAME"),
										  result.getString("LAST_NAME"),
										  result.getInt("ID"),
										  listCourses);
			}
			Message response = new Message(0, foundUser);
			return response;
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE SEARCHING AASSIGNMENT TABLE");
			e.printStackTrace();
		}
		return new Message(0, null); //Could not find user with corresponding ID
	}
	
	/**
	 * Searhes User table for a student user with matching last name
	 * (Doesn't populate list of courses)
	 * @param lastName the lastName
	 * @return Message containing list of users
	 */
	public Message searchStudentUserByLastName(String lastName) {
		ArrayList<StudentUser> listStudents = new ArrayList<StudentUser>();
		String query = "SELECT * FROM " + DB_NAME  + "." + USER_TABLE +
					   " WHERE LAST_NAME LIKE " + "'" +  lastName + "'";
		try {
			statement = connection.createStatement();
			ResultSet results = statement.executeQuery(query);
			while(results.next()) {
				StudentUser temp = new StudentUser(results.getString("TYPE"),
												   results.getString("FIRST_NAME"),
												   results.getString("LAST_NAME"),
												   results.getInt("ID"),
												   null);
				listStudents.add(temp);
			}	
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE SEARCHING USERS BY LASTNAME");
			e.printStackTrace();
		}
		
		Message response = new Message(0, listStudents);
		return response;
	}
	
	/**
	 * Constructs a Course object corresponding to a certain courseID
	 * @param courseID the courseID
	 * @return Message Contains the required Course Object
	 */
	public Message constructCourseByCourseID(int courseID) {
		String query = "SELECT * FROM " + DB_NAME + "." + COURSE_TABLE + 
						" WHERE ID=" + courseID;
		try {
			statement = connection.createStatement();
			ResultSet results = statement.executeQuery(query);
			if(results.next()) {
				//Find all CourseAssignmemnts related to this course
				Message assignmentsResponse  = searchAssignmentTableByCourseId(courseID);
				ArrayList<CourseAssignment> assignmentList = 
									(ArrayList<CourseAssignment>)assignmentsResponse.getData();
				//Construct required course object
				Course course = new Course(courseID,
										 results.getString("NAME"),
										 results.getInt("PROF_ID"),
										 results.getInt("ACTIVE"),
										 assignmentList);
				Message response = new Message(0, course);
				return response;
			}	
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE SEARCHING COURSE TABLE BY COURSE_ID");
			e.printStackTrace();
		}
		return new Message(0, null);
	}
	
	/**
	 * Searches Assignment table for all assignments relates=d to a certain course 
	 * @param courseId ID of course
	 * @return Message object containing a list of Assignment objects
	 */
	public Message searchAssignmentTableByCourseId(int courseId) {
		ArrayList<CourseAssignment> assignmentList = new ArrayList<CourseAssignment>();
		String query = "SELECT * FROM " + DB_NAME +"." + ASSIGNMENT_TABLE +
					   " WHERE COURSE_ID=" + courseId;
		try {
			statement = connection.createStatement();
			ResultSet results = statement.executeQuery(query);
			while(results.next()) {
				String fileName = results.getString("PATH");
				String path = fileWorker.buildPath(fileName);
				byte[] retrievedFile = fileWorker.retrieveFileContents(path);
				CourseAssignment temp = new CourseAssignment(results.getInt("ID"),
															 courseId,
															 results.getString("TITLE"),
															 results.getInt("ACTIVE"),
															 results.getString("DUE_DATE"),
															 retrievedFile); 
				assignmentList.add(temp);
			}
		}
		catch(SQLException e) {
			System.err.println("Error in searching assignment table");
			e.printStackTrace();
		}
		Message response = new Message(0, assignmentList);
		return response;	
	}
		
	/**
	 * Searches the student enrollment table by CourseID -- mainly to get all students enrolled in a course
	 * @param courseId the courseID
	 * @return Message containing a list of Enrollment Objects or null if key can't be found
	 */
	public Message searchStudentEnrollmentTableByCourseId(int courseId) {
		ArrayList<Enrollment> enrollList = new ArrayList<Enrollment>();
		String query = "SELECT * FROM " + DB_NAME + "." + STUDENT_ENROLL_TABLE +
					   " WHERE COURSE_ID= " + courseId;
		try {
			statement = connection.createStatement();
			ResultSet results = statement.executeQuery(query);
			while(results.next()) {
				Enrollment temp  = new Enrollment(results.getInt("ID"),
												  results.getInt("STUDENT_ID"),
												  results.getInt("COURSE_ID"));
				enrollList.add(temp);
			}
		}
		catch(SQLException e) {
			System.err.println("ERROR SEARCHING ENROLLMENT TABLE");
			e.printStackTrace();
		}
		Message response = new Message(0, enrollList);
		return response;
	}
	
	/**
	 * Searches Course table and builds courses that match professor ID
	 * @param profId the professor's ID
	 * @return Message containing a list of all courses the professor teaches
	 */
	public Message searchforCoursesByProfID(int profId) {
		
		ArrayList<Course> courseList = new ArrayList<Course>();
		String query = "SELECT * FROM " + DB_NAME + "." + COURSE_TABLE +
						" WHERE PROF_ID = " + profId;
		try {
			//Find all courses that professor teaches
			statement = connection.createStatement();
			ResultSet results = statement.executeQuery(query);
			while(results.next()) {
				//Find assignments for each course found
				int courseId = results.getInt("ID");
				Message assignmentsResponse  = searchAssignmentTableByCourseId(courseId);
				ArrayList<CourseAssignment> assignmentList = 
									(ArrayList<CourseAssignment>)assignmentsResponse.getData();
				Course temp = new Course(courseId,
										 results.getString("NAME"),
										 results.getInt("PROF_ID"),
										 results.getInt("ACTIVE"),
										 assignmentList);
				courseList.add(temp);
			}
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE SEARCHING COURSE TABLE");
			e.printStackTrace();
		}
		Message response = new Message(0, courseList);
		return response;
	}
	
	/**
	 * Constructs a list of courses taught by a specific prof
	 * @param prof_ID ID of the specific prof
	 * @return Message containing a list of ProfessorCourse objects 
	 */
	public Message constructProfessorCourses(int prof_ID) {
		ArrayList<ProfessorCourse> professorCourseList = new ArrayList<ProfessorCourse>();
		
		//Find all "regular version" of courses prof teaches
		Message courseListMessage = searchforCoursesByProfID(prof_ID);
		ArrayList<Course> courseList = (ArrayList<Course>)courseListMessage.getData();
		for(int i = 0; i < courseList.size(); i++) {	
			//Find list of enrolled students for each course found
			Message resultFromEnrollment = searchStudentEnrollmentTableByCourseId(courseList.get(i).getCourseID());
			ArrayList<Enrollment> enrollList = (ArrayList<Enrollment>)resultFromEnrollment.getData();
			ArrayList<StudentUser> studentList = new ArrayList<StudentUser>();
			for(int j = 0; j < enrollList.size(); j++) {
				Message studentUserMessage = constructStudentUser((Integer)enrollList.get(j).getStudentID());
				if(studentUserMessage.getData() != null)
					studentList.add((StudentUser)studentUserMessage.getData());
			}
			//Construct professor course
			ProfessorCourse temp = new ProfessorCourse(courseList.get(i).getCourseID(),
					courseList.get(i).getCourseName(), courseList.get(i).getProfId(), courseList.get(i).isActive(), 
					courseList.get(i).getAssignments(), studentList);
			professorCourseList.add(temp); //Add course to list of courses
		}
		
		Message response = new Message(0, professorCourseList);
		return response;
	}
	
	/**
	 * Fetches all submissions matching an assignment ID
	 * @param assignId - assignment ID 
	 * @return a valid Message object whose data contains a list of all matching submissions
	 */
	public Message searchSubmissionTable(int assignId) {
		//Fetch all submissions matching an assignement ID
		ArrayList<Submission> submissionResultList = new ArrayList<Submission>();
		String query = "SELECT * FROM " +
						DB_NAME + "." + SUBMISSION_TABLE +
						" WHERE ASSIGN_ID =" + assignId;
		try {
			statement = connection.createStatement();
			ResultSet results = statement.executeQuery(query);
			while(results.next()) {
				String fileName = results.getString("PATH");
				String path = fileWorker.buildPath(fileName);
				byte[] fileContents = fileWorker.retrieveFileContents(path);
				Submission temp = new Submission(results.getInt("ID"),
												 results.getInt("STUDENT_ID"),
												 results.getInt("ASSIGN_ID"),
												 fileContents,
												 results.getString("TITLE"),
												 results.getString("COMMENTS"),
												 results.getString("TIME_STAMP"),
												 results.getInt("GRADE")
												 );
				submissionResultList.add(temp);
			}
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE SEARCHING SUBMISSIONS");
			e.printStackTrace();
		}
		Message response = new Message(0, submissionResultList);
		return response;
	}
	
	/**
	 * Fetches all submissions matching an assignment ID
	 * @param assignId - assignment ID 
	 * @param studentId - the studentId
	 * @return a valid Message object whose data contains a list of all matching submissions
	 */
	public Message searchSubmissionTableByAssignIdAndStudId(int assignId, int studentId) {
		//Fetch all submissions matching an assignement ID
		ArrayList<Submission> submissionResultList = new ArrayList<Submission>();
		String query = "SELECT * FROM " +
						DB_NAME + "." + SUBMISSION_TABLE +
						" WHERE ASSIGN_ID =" + assignId + " and " + 
						"STUDENT_ID=" + studentId;
		try {
			statement = connection.createStatement();
			ResultSet results = statement.executeQuery(query);
			while(results.next()) {
				String fileName = results.getString("PATH");
				String path = fileWorker.buildPath(fileName);
				byte[] fileContents = fileWorker.retrieveFileContents(path);
				Submission temp = new Submission(results.getInt("ID"),
												 results.getInt("STUDENT_ID"),
												 results.getInt("ASSIGN_ID"),
												 fileContents,
												 results.getString("TITLE"),
												 results.getString("COMMENTS"),
												 results.getString("TIME_STAMP"),
												 results.getInt("GRADE")
												 );
				submissionResultList.add(temp);
			}
			return new Message(0, submissionResultList);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE SEARCHING SUBMISSIONS");
			e.printStackTrace();
		}
		return new Message(0, null);
	}
	
	//-------------------------------------------------------------------------------------------------------------
	//---------------------FUNCTIONS TO ADD/DELETE ENTRIES---------------------------------------------------------
	//-------------------------------------------------------------------------------------------------------------
	
	public void addUser(User toAdd) {
		//NOT REQUIRED AS AT NOW
		//Will worry about later				   
	}
	
	public void deleteUser(User toDelete) {
		//NOT Necessary for application
		//Will worry about later
	}
	
	/**
	 * Adds a newly created course  to the Database
	 * @param toAdd course to be added
	 * @return A valid Message object on success, a null Message object on failure
	 */
	public Message addNewCourses(Course toAdd) {
		String query = "INSERT INTO " + 
						DB_NAME + "." + COURSE_TABLE +
						" values(" +
						toAdd.getCourseID() + ", " +
						toAdd.getProfId() + ", '" +
						toAdd.getCourseName() + "', " +
						toAdd.isActive() + 
						")";
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			return new Message(0, toAdd);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE TRYING TO ADD TO COURSE TABLE");
			e.printStackTrace();
		}	
		return new Message(0, null);
	}
	
	public void deleteFromCourses(Course toDelete) {
		//Not necessary for application
		//Might also want to delete from assignments and all other tables
	}
	
	public Message addNewAssignments(CourseAssignment toAdd) {
		String path = fileWorker.buildPath(toAdd.getTitle());
		String query = "INSERT INTO " +
						DB_NAME + "." + ASSIGNMENT_TABLE +
						" values(" +
						toAdd.getAssignID() + ", " + 
						toAdd.getCourseID() + ", '" +
						toAdd.getTitle() + "', " +
						"'" + toAdd.getTitle() + "'" + ", " +
						toAdd.isActive() + ", '" +
						toAdd.getDueDate() + "'" +
						")";
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			fileWorker.storeFile(toAdd.getAssignmentFile(), path);
			return new Message(0, toAdd);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE TRYING TO ADD TO ASSIGNMENT TABLE");
			e.printStackTrace();
		}
		return new Message(0, null);				
	}
	
	public void deleteFromAssignments(CourseAssignment toDelete) {
		//Not necessary for application
	}
	
	/**
	 * Records/Adds a new enrollment to enrollment table
	 * @param toAdd the Enrollment object to add
	 * @return a valid Message object if successful, a null Message object otherwise
	 */
	public Message addNewEnrollment(Enrollment toAdd) {
		String query = "INSERT INTO " +
						DB_NAME + "." + STUDENT_ENROLL_TABLE +
						" values(" +
						toAdd.getEnrollmentID() + ", " + 
						toAdd.getStudentID() + ", " +
						toAdd.getCourseID() + 
						")";	
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			return new Message(0, toAdd);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE TRYING TO ADD TO ENROLLMENT TABLE");
			e.printStackTrace();
		}	
		return new Message(0, null);
	}
	
	/**
	 * Un-enrolls/Deletes a studnt's enrollment object from Enrollment table
	 * @param toDelete enrollment object to delete
	 * @return a valid Message object on success, a null Message object otherwise
	 */
	public Message deleteFromEnrollment(Enrollment toDelete) {
		String query = "DELETE FROM " + DB_NAME + "." + STUDENT_ENROLL_TABLE + 
					   " WHERE STUDENT_ID=" + toDelete.getStudentID() + " and " +
					   " COURSE_ID=" + toDelete.getCourseID();
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			return new Message(0, toDelete);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE TRYING TO DELETE FOROM ENROLLMENT TABLE");
			e.printStackTrace();
		}
		return new Message(0, null);
	}
	
	/**
	 * Records/Adds a new  assignment Submission to Submission Table
	 * @param toAdd Submission object to add
	 * @return a valid Message object if successful, a null Message object otherwise
	 */
	public Message addNewSubmission(Submission toAdd) {
		
		String path = fileWorker.buildPath(toAdd.getTitle()); 
		String query = "INSERT INTO " +
						DB_NAME + "." + SUBMISSION_TABLE +
						" values(" +
						toAdd.getSubmissionID() + ", " + 
						toAdd.getAssignmentID() + ", " +
						toAdd.getUserID() + ", '" + 
						toAdd.getTitle() + "', " + " '" +
						toAdd.getTitle() + "', " +
						"'" + toAdd.getComment() + "', " +  "'" +
						toAdd.getTimeStamp() + "', " + 
						toAdd.getAssignmentGrade() + ")";
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			fileWorker.storeFile(toAdd.getSubmissionFile(), path);
			return new Message(0, toAdd);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE TRYING TO ADD TO SUBMISSION TABLE");
			e.printStackTrace();
		}
		return new Message(0, null);				
	}
	
	/**
	 * Deletes a submission form submission table
	 * @param toDelete submission to be deleted
	 * @return A valid message object on success, a nul message object on failure
	 */
	public Message deleteFromSubmission(Submission toDelete) {
		String query = "DELETE FROM " + DB_NAME + "." + SUBMISSION_TABLE +
						" WHERE STUDENT_ID="  + toDelete.getUserID()+ 
						" and ASSIGN_ID=" + toDelete.getAssignmentID();
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			return new Message(0, toDelete);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE TRYING TO DELETE FROM ENROLLMENT");
			e.printStackTrace();
		}
		return new Message(0, null);
	}
	
	//------------------------------------------------------------------------------------------
	//---------------FUNCTIONS TO MODIFY EXISTING TABLE ENTRIES------------------------------------
	//------------------------------------------------------------------------------------------
	public void modifyUserTable(User user, int id) {
		//NOT NECESSARY
	}
	
	/**
	 * Modifies an already existing entry in the Assignment table
	 * @param assign CourseAssignment object that specifies row to modify
	 * @return a Message with an assignment object if successful, otherwise, a Message with null
	 */
	public Message modifyAssignmentTable(CourseAssignment assign) {
		String query = "UPDATE " + DB_NAME +"." +  ASSIGNMENT_TABLE +
					   " SET ACTIVE=" + assign.isActive() +
					   " WHERE ID=" + assign.getAssignID();
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			return new Message(0, assign);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE TRYING TO UPDATE ASSIGNMENT TABLE");
			e.printStackTrace();
		}
		return new Message(0, null);
	}
	
	/**
	 * Modifies an already existing entry in the Course table
	 * @param course The course to be modified
	 * @return a Message with a course object if the update was successful, otherwise a Message with null
	 */
	public Message modifyCourseTable(Course course) {
		String query = "UPDATE " + DB_NAME + "." + COURSE_TABLE +
				   " SET ACTIVE=" + course.isActive() +
				   " WHERE ID=" + course.getCourseID();
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			return new Message(0, course);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE TRYING TO UPDATE COURSE TABLE");
			e.printStackTrace();
		}
		return new Message(0, null);
	}
	
	/**
	 * Modify and already existing entry in the Submission table
	 * @param submission Suubmission object containing info about entry to be modified
	 * @return A valid Message on success, a null Message on failure
	 */
	public Message modifySubmissionTable(Submission submission) {
		String query = "UPDATE " + DB_NAME + "." + SUBMISSION_TABLE +
					   " SET " +
					   "COMMENTS='" + submission.getComment() + "', " +
					   "GRADE=" + submission.getAssignmentGrade() + 
					   " WHERE ASSIGN_ID=" + submission.getAssignmentID() +
					   " and STUDENT_ID=" + submission.getUserID();
		try {
			statement = connection.createStatement();
			statement.executeUpdate(query);
			return new Message(0, submission);
		}
		catch(SQLException e) {
			System.err.println("ERROR WHILE TRYING TO MODIFY SUBMISSIONS");
			e.printStackTrace();
		}
		return new Message(0, null);
	}
}
