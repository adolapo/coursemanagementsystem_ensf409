package serverPackage;

import java.util.ArrayList;
import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

/**
 * This class allows for the sending of emails.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @since April 11, 2018
 * @version 1.0
 */
class EmailHelper {
	/**
	 * The password used for all emails
	 */
	private static String PASSWORD = "409coursemana";
	
	/**
	 * Sends an email based on the given parameters
	 * @param sender the sender of the email
	 * @param content the content of the email
	 * @param subject the subject of the email
	 * @param receivers the receivers of the email
	 * @return false if the email cannot be sent. Otherwise true
	 */
	boolean sendEmail(String sender, String content, String subject, ArrayList<String> receivers) {
		if(receivers.size() < 1)
			return false;
		Properties properties = new Properties();
		properties.put("mail.smtp.starttls.enable", "true"); // Using TLS
		properties.put("mail.smtp.auth", "true"); // Authenticate
		properties.put("mail.smtp.host", "smtp.gmail.com"); // Using Gmail Account
		properties.put("mail.smtp.port", "587"); // TLS uses port 587
		
		Session session = Session.getInstance(properties,
				new javax.mail.Authenticator(){
				 protected PasswordAuthentication getPasswordAuthentication() {
				 return new PasswordAuthentication(sender, PASSWORD);
				 }
				});
		try {
			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(sender));
			for(String e : receivers) {
				message.addRecipient(Message.RecipientType.BCC, new InternetAddress(e));
			}
			message.setSubject(subject);
			message.setText(content);
			
			Transport.send(message);
		}catch(MessagingException e) {
			e.printStackTrace();
			return false;
		}
		return true;
	}
}
