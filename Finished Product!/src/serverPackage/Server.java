package serverPackage;

import java.net.Socket;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.ServerSocket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Class to manage client servicing
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @since April 3rd, 2018
 * @version 1.0
 */
class Server implements data.Requestcodes{
	/**
	 * Socket to client
	 */
	private Socket theSocket;
	/**
	 * ServerSocket object necessary for initializing server
	 */
	private ServerSocket serverSocket;
	/**
	 * Output stream to client
	 */
	private ObjectOutputStream clientOut;
	/**
	 * Input Stream from client
	 */
	private ObjectInputStream clientIn;
	/**
	 * Worker threads
	 */
	private ExecutorService threadWorkers;
	
	/**
	 * Default constructor
	 */
	public Server() {
		try{
			serverSocket = new ServerSocket(9898);
			threadWorkers = Executors.newCachedThreadPool();
		}
		catch(IOException e) {
			System.err.println("Unable to create Server");
			e.printStackTrace();
		}
	}
	
	/**
	 * Server run function
	 */
	public void runServer() {
		System.out.println("Server started running");
		while(true) {
			try{
				theSocket = serverSocket.accept();
				System.out.println("Accepted a client...");
				clientIn  = new ObjectInputStream(theSocket.getInputStream());
				clientOut = new ObjectOutputStream(theSocket.getOutputStream());
				threadWorkers.execute(new Manager(clientIn, clientOut));
			
			}
			catch (IOException e) {
				System.err.println("Server error while trying to connect to client");
				e.printStackTrace();
			}
		}
	}
	public static void main(String[] args) {
		Server mainMan = new Server();
		mainMan.runServer();

	}

}
