/**
 * 
 */
package data;

import java.util.ArrayList;
/**
 * A java data structure that represents a Student User in a client management system.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @since April 3, 2018
 * @version 1.0
 *
 */
public class StudentUser extends User {
	
	/**
	 * The serial version ID for serializing StudentUser
	 */
	private static final long serialVersionUID = 1L;
	/**
	 * The list of courses a student is enrolled in
	 */
	private ArrayList<Course> courseList;

	/**
	 * Constructs a StudentUser based on the given parameters
	 * @param type the type of the user
	 * @param firstName the first name of the user
	 * @param lastName the last name of the user
	 * @param userID the user's ID
	 * @param courseList the list of courses for th estudent
	 */
	public StudentUser(String type, String firstName, String lastName, int userID, 
			ArrayList<Course> courseList) {
		super(type, firstName, lastName, userID);
		this.courseList = courseList;
	}
	
	/**
	 * Gets the courseList
	 * @return the courseList
	 */
	public ArrayList<Course> getCourseList() {
		return courseList;
	}

	/**
	 * Sets the courseList based on the desired value
	 * @param courseList the courseList to set
	 */
	public void setCourseList(ArrayList<Course> courseList) {
		this.courseList = courseList;
	}
	
	public String toString() {
		String s = "Last Name: " + lastName + "  |  ID: " + userID;
		return s;
	}
}
