package data;
import java.io.Serializable;

/**
 * A java data stucture that represents a student's enrollment in a course.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @since April 3, 2018
 * @version 1.0
 *
 */
public class Enrollment implements Serializable{
	/**
	 * The serial version ID for serializing Enrollment
	 */
	private static final long serialVersionUID = -3276844101153856739L;
	/**
	 * The ID of the student enrolled
	 */
	private int studentID;
	/**
	 * The ID of the course the student is enrolled in
	 */
	private int courseID;
	/**
	 * The Id of this particular enrollment
	 */
	private int enrollmentID;
	
	/**
	 * Constructs an Enrollment object based on the given parameters
	 * @param enrollID the ID for the enrollment
	 * @param studentID the desired student ID
	 * @param courseID the desired course ID
	 */
	public Enrollment(int enrollID, int studentID, int courseID) {
		super();
		this.enrollmentID = enrollID;
		this.studentID = studentID;
		this.courseID = courseID;
	}
	
	
	/**
	 * @return the enrollmentID
	 */
	public int getEnrollmentID() {
		return enrollmentID;
	}


	/**
	 * @param enrollmentID the enrollmentID to set
	 */
	public void setEnrollmentID(int enrollmentID) {
		this.enrollmentID = enrollmentID;
	}


	/**
	 * Gets the ID of the student
	 * @return studentID
	 */
	public int getStudentID() {
		return studentID;
	}

	/**
	 * Sets the Id of the student to the desired value
	 * @param studentID the student ID desired
	 */
	public void setStudentID(int studentID) {
		this.studentID = studentID;
	}

	/**
	 * Gets the course ID of th enrollment
	 * @return courseID
	 */
	public int getCourseID() {
		return courseID;
	}

	/**
	 * Sets the ID of the ocurse to the desired value
	 * @param courseID the course ID desired
	 */
	public void setCourseID(int courseID) {
		this.courseID = courseID;
	}
	
}
