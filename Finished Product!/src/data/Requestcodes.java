/**
 * 
 */
package data;

/**
 * Contains codes to be used by a client server course management system.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @since April 3, 2018
 * @version 1.0
 *
 */
public interface Requestcodes {
	/**
	 * sending: Enrollment object with studentID and courseID
	 * expecting: an Enrollment object if valid enrollment, else null
	 */
	public static final int ENROLLMENT = 0;
	/**
	 * sending: a User
	 * expecting: a User
	 */
	public static final int USER = 1;
	/**
	 * sending: A User with type "S", and an ID
	 * expecting: a Student User with everything
	 */
	public static final int STUDENT_USER = 2;
	/**
	 * sending: a submission object
	 * expecting: a submission object if successful, null if not successful
	 */
	public static final int SUBMISSION = 3;
	/**
	 * sending: User with ID
	 * expecting: a list of ProfessorCourses
	 */
	public static final int PROFESSOR_COURSE = 4;
	/**
	 * sending: A Course Assignment
	 * expecting: nothing
	 */
	public static final int ASSIGNMENT = 5;
	/**
	 * sending: Email object
	 * expecting: nothing
	 */
	public static final int EMAIL = 6;
	/**
	 * sending: a Login object with ID and password
	 * expecting: a User object
	 */
	public static final int LOGIN = 7;
	/**
	 * sending: A StudentUser with ID field 
	 * expecting: a StudentUser with name and ID fields
	 */
	public static final int SEARCH_STUDENT_ID = 8;
	/**
	 * sending: A Student User with lastName field
	 * expecting: a list of StudentUsers with name and ID fields
	 */
	public static final int SEARCH_STUDENT_LN = 9;
	/**
	 * sending: Enrollment object with studentID and courseID
	 * expecting: nothing
	 */
	public static final int UNENROLLMENT = 10;
	/**
	 * sending: CourseAssignment with updated isActive
	 * expecting: nothing
	 */
	public static final int UPDATE_ASSIGNMENT = 11;
	/**
	 * sending: A Submission object with a specified assignmentID
	 * expecting: a list of Submissions
	 */
	public static final int GET_SUBMISSION = 12;
	/**
	 * sending: A Submission object with a new comment
	 * expecting: nothing
	 */
	public static final int UPDATE_SUBMISSION = 13;
	/**
	 * sending: A course object
	 * expecting: nothing
	 */
	public static final int UPDATE_COURSE = 14;
	/**
	 * sending: A course object
	 * expecting: a course object
	 */
	public static final int ADD_COURSE = 15;
	/**
	 * sending: A submission object with assignment ID and student ID
	 * expecting: a submission object if it exists, otherwise, null
	 */
	public static final int VERIFY_SUBMISSION = 16;
}
