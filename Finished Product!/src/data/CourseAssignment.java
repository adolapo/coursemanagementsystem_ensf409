/**
 * 
 */
package data;

import java.io.File;
import java.io.Serializable;

/**
 * A java data structure that contains the fields and methods to represent a Course Assignment in a 
 * course management system.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @since April 3, 2018
 * @version 1.0
 *
 */
public class CourseAssignment implements Serializable {

	/**
	 * The serial version ID for serializing CourseAssignment
	 */
	private static final long serialVersionUID = 8817892491004020112L;
	/**
	 * The assignment ID
	 */
	private int assignID;
	
	/**
	 * The course the assignment corresponds to
	 */
	private int courseID;
	/**
	 * The title of the assignment
	 */
	private String title;
	/**
	 * whether the assignment is active or inactive
	 */
	private int isActive;
	/**
	 * The due date for the assignment
	 */
	private String dueDate;
	/**
	 * The assignment file
	 */
	private byte[] assignmentFile;
	
	
	/**
	 * Creates a CourseAssignment object based on the given parameters
	 * @param assignId the ID of the assignment
	 * @param courseID the ID of the course 
	 * @param title the title of the assignment
	 * @param isActive whether it is active or not
	 * @param dueDate the duedate of the assignment
	 * @param assignmentFile the assignment file
	 */
	public CourseAssignment(int assignId, int courseID, String title, int isActive, String dueDate, byte[] assignmentFile) {
		this.assignID = assignId;
		this.courseID = courseID;
		this.title = title;
		this.isActive = isActive;
		this.dueDate = dueDate;
		this.assignmentFile = assignmentFile;
	}


	/**
	 * Gets the courseID
	 * @return the courseID
	 */
	public int getCourseID() {
		return courseID;
	}


	/**
	 * Sets the courseID based on the desired value
	 * @param courseID the courseID to set
	 */
	public void setCourseID(int courseID) {
		this.courseID = courseID;
	}


	/**
	 * Gets the title
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}


	/**
	 * Sets the title based on the desired value
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}


	/**
	 * Gets the isActive
	 * @return the isActive
	 */
	public int isActive() {
		return isActive;
	}


	/**
	 * Sets the isActive based on the desired value
	 * @param isActive the isActive to set
	 */
	public void setActive(int isActive) {
		this.isActive = isActive;
	}


	/**
	 * Gets the dueDate
	 * @return the dueDate
	 */
	public String getDueDate() {
		return dueDate;
	}


	/**
	 * Sets the dueDate based on the desired value
	 * @param dueDate the dueDate to set
	 */
	public void setDueDate(String dueDate) {
		this.dueDate = dueDate;
	}


	/**
	 * Gets the assignmentFile
	 * @return the assignmentFile
	 */
	public byte[] getAssignmentFile() {
		return assignmentFile;
	}


	/**
	 * Sets the assignmentFile based on the desired value
	 * @param assignmentFile the assignmentFile to set
	 */
	public void setAssignmentFile(byte[] assignmentFile) {
		this.assignmentFile = assignmentFile;
	}
	
	/**
	 * Fetches assignment ID
	 * @return required assignment ID
	 */
	public int getAssignID() {
		return assignID;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		String s = title + "   |   Due: " + dueDate + "   |   ";
		if(isActive == 1)
			s+= "Active";
		else{
			s+= "Inactive";
		}
		return s;
	}
	
	
}
