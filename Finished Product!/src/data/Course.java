/**
 * 
 */
package data;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * A java data structure that contains the fields and methods to represent a Course in a 
 * course management system.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @since April 3, 2018
 * @version 1.0
 *
 */
public class Course implements Serializable {

	/**
	 * The serial version ID for serializing Course
	 */
	private static final long serialVersionUID = -7464120887450960027L;
	/**
	 * The ID of the course
	 */
	protected int courseID;
	/**
	 * The name of the course
	 */
	protected String courseName;
	/**
	 * The name of the professor of the course
	 */
	protected int profId;
	/**
	 * Indicates whether the course is active or not
	 */
	protected int isActive;
	/**
	 * Assignments in the course
	 */
	protected ArrayList<CourseAssignment> assignments;
	
	/**
	 * Creates a Course object based on the given parameters.
	 * @param courseID the desired courseID
	 * @param courseName the desired name of the course
	 * @param profId the ID of the prof
	 * @param isActive the desired active state of the course
	 * @param assignments the assignments in the course
	 */
	public Course(int courseID, String courseName, int profId, int isActive, 
			ArrayList<CourseAssignment> assignments) {
		this.courseID = courseID;
		this.courseName = courseName;
		this.profId = profId;
		this.isActive = isActive;
		this.assignments = assignments;
	}

	/**
	 * Gets the courseID
	 * @return the courseID
	 */
	public int getCourseID() {
		return courseID;
	}

	/**
	 * Sets the courseID based on the desired value
	 * @param courseID the courseID to set
	 */
	public void setCourseID(int courseID) {
		this.courseID = courseID;
	}

	/**
	 * Gets the courseName
	 * @return the courseName
	 */
	public String getCourseName() {
		return courseName;
	}

	/**
	 * Sets the courseName based on the desired value
	 * @param courseName the courseName to set
	 */
	public void setCourseName(String courseName) {
		this.courseName = courseName;
	}

	/**
	 * Gets the profId
	 * @return the profName
	 */
	public int getProfId() {
		return profId;
	}

	/**
	 * Sets the profId based on the desired value
	 * @param profId the profName to set
	 */
	public void setProfName(int profId) {
		this.profId = profId;
	}

	/**
	 * Gets whether the course  is active
	 * @return  isActive
	 */
	public int isActive() {
		return isActive;
	}

	/**
	 * Sets wether the course is active/inactive based on the desired value
	 * @param isActive the active state to set
	 */
	public void setActive(int isActive) {
		this.isActive = isActive;
	}

	/**
	 * Gets the assignments
	 * @return the assignments
	 */
	public ArrayList<CourseAssignment> getAssignments() {
		return assignments;
	}

	/**
	 * Sets the assignments based on the desired value
	 * @param assignments the assignments to set
	 */
	public void setAssignments(ArrayList<CourseAssignment> assignments) {
		this.assignments = assignments;
	}
	
	@Override
	public String toString() {
		String s = "Course ID: " + String.valueOf(courseID) + "  |  Name: " + courseName;
		return s;
	}
}
