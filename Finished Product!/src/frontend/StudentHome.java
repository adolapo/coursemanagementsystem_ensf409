package frontend;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import data.Course;

/**
 * This view allows the student to see all active courses they are enrolled in.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @version 1.0
 * @since April 8, 2018
 *
 */
public class StudentHome extends JPanel {
	/**
	 * The model for the list of courses
	 */
	private DefaultListModel <Course> model = new DefaultListModel<>();	
	/**
	 * The list of courses
	 */
	JList<Course> list;
	
	/**
	 * Creates the panel for the home screen based on the given list of courses
	 * @param courseList the courses to be listed
	 */
	public StudentHome(ArrayList<Course> courseList) {
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrollPane, BorderLayout.CENTER);
		
		list = new JList<>();
		list.setModel(model);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setFont(new Font("Arial",Font.BOLD,16));
		list.setFixedCellHeight(25);
		list.setBorder(new EmptyBorder(10,10, 10, 10));
		scrollPane.setViewportView(list);
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		
		updateCourses(courseList);
	}
	
	/**
	 * Sets the listener for list
	 * @param l the desired listener
	 */
	void setListListener(ListSelectionListener l) {
		list.addListSelectionListener(l);
	}
	
	/**
	 * Updates the model for list of courses
	 * @param courseList the courses to be listed
	 */
	void updateCourses(ArrayList<Course> courseList) {
		model.clear();
		for(Course e : courseList){
	        model.addElement(e);
	     }
	}
	
	/**
	 * Gets the model
	 * @return the model
	 */
	DefaultListModel<Course> getModel() {
		return model;
	}
}
