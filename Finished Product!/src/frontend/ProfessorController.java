/**
 * 
 */
package frontend;

import java.awt.Component;
import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import data.Course;
import data.CourseAssignment;
import data.Email;
import data.Enrollment;
import data.Message;
import data.ProfessorCourse;
import data.StudentUser;
import data.Submission;
import data.User;

/**
 * A class that controls events from a view and communicates with server.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @version 1.0
 * @since April 5, 2018
 *
 */
class ProfessorController extends ClientController {
	/**
	 * The GUI the controller receives events from
	 */
	private ProfessorView view;
	/**
	 * The user that has logged in.
	 */
	User user;
	
	/**
	 * Creates a controller based on the given parameters. 
	 * @param socketIn the input stream for the client
	 * @param socketOut the output stream for the client
	 * @param user the user that has logged in
	 */
	public ProfessorController(ObjectInputStream socketIn, ObjectOutputStream socketOut, User user) {
		super(socketIn, socketOut);
		this.user = user;	
	}
	
	@Override
	protected void setUpView() {
		String name = user.getFirstName() + " "+ user.getLastName();
		String id = String.format("%08d", user.getUserID());
		
		ArrayList<ProfessorCourse> courses = getCourses();
		Home home = new Home(courses);
		
		// Launches GUI when everything has finished setting up
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					view = new ProfessorView(home);
					view.setLblName(name);
					view.setLbID(id);
					view.setHomeListeners(new HomeListListener(), new HomeAddCourseListener());
					view.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});	
	}	
	
	/**
	 * Gets the courses the user has
	 * @return the list of courses the user has
	 */
	 ArrayList<ProfessorCourse> getCourses() {
		Message m = new Message(PROFESSOR_COURSE, user);
		sendMessage(m);
		m = (Message)receiveMessage();
		ArrayList<ProfessorCourse> courses = (ArrayList<ProfessorCourse>)m.getData();
		return courses;
	}
	
	/**
	 * Updates the courses
	 */
	 void updateCourses() {
			ArrayList<ProfessorCourse> courses = getCourses();
			DefaultListModel<ProfessorCourse> model = view.getHome().getModel();
			model.clear();
	        for(ProfessorCourse e : courses){
	          model.addElement(e);
	          if(view.cv != null) {
		          if(e.getCourseID() == view.cv.course.getCourseID()) {
		        	  	view.cv.course = e;
		          }
	          }
	        }
		}
	
	/**************************************************************************/
	/**************************************************************************/
	/**********************************LISTENERS*******************************/
	/**************************************************************************/
	/**************************************************************************/
	
	 /**
	  * Listener for viewing students button in course view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class ViewStudentsListener implements ActionListener{

		@Override
		public void actionPerformed(ActionEvent e) {
			// TODO Auto-generated method stub
			ArrayList<StudentUser> su = view.cv.course.getStudentList();
			view.lblLocationLabel.setText(view.cv.course.getCourseName() + ": Students");
			view.cv.studentList(new StudentListListener(), su);
		}
		
	}
	
	/**
	  * Listener for the list of courses in home view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class HomeListListener implements ListSelectionListener
	{
		@Override
	    public void valueChanged(ListSelectionEvent e) {
	      if (e.getValueIsAdjusting() == false) {
	        JList<ProfessorCourse> list = view.getHome().list;
	        ProfessorCourse sel = list.getSelectedValue();
	        // Make sure course exists
	        if (sel != null) {
	        		view.launchCourseView(sel, new StudentListListener());
	        		view.setCourseViewListeners(new CVActivateDeactivateListener(), new CVNewAssignment(), 
	        				new CVSearchStudentListener(), new CVEnrollUnenrollListener(), new CVEmailListener(), 
	        				new CVAssignmentsListener(), new ViewStudentsListener());
	        }  
	        list.clearSelection();
	      }
	    }
	}
	
	/**
	  * Listener for adding course button in home view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class HomeAddCourseListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			String name = view.getHome().getCourseNameField();
			if(name == null || name.equals("")) {
				view.displayErrorMessage("Please enter a valid course name");
				return;
			}
			
			int i = 0;
			// Finding a valid ID for new insertion
			while(i < 99999999) {
				Course course = new Course(i, name, user.getUserID(), 0, null);
				Message m2 = new Message(ADD_COURSE, course);
				sendMessage(m2);
				m2 = receiveMessage();
				if(m2.getData() != null)
					break;
				i++;
			}
			if (i > 99999999) {
				view.displayErrorMessage("Can't add course");
			}
			else {
				view.displayMessage("Course added successfully", "Added");
				updateCourses();
			}
			
		}
	}
	
	/**
	  * Listener for search students button in course view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class CVSearchStudentListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			view.studentSearch(new StudentSearchListener(), new StudentListListener());
		}
	}
	
	/**
	  * Listener for send email button in course view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class CVEmailListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			view.email(new SendEmailListener());
		}
	}
	
	/**
	  * Listener for assignments button in course view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class CVAssignmentsListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			view.listAssignments(new AssignmentListListener());
		}
	}
	
	/**
	  * Listener for done button in submission pop up
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class SPUDoneListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			Submission s = view.cv.av.spu.s;
			String g = view.cv.av.spu.getGrade();
			String c = view.cv.av.spu.getComment();
			if(g == null || c == null) {
				view.displayErrorMessage("Invalid grade or comment");
				return;
			}
			if(g.equals("") || c.equals("")) {
				view.displayErrorMessage("Invalid grade or comment");
				return;
			}
			int grade = 0;
			System.out.println(g);
			if(g.equals("---"))
				grade = -1;
			else {
				try {
					grade = Integer.parseInt(g);
				}catch(NumberFormatException e) {
					view.displayErrorMessage("Invalid grade");
					return;
				}
			}
			
			s.setComment(c);
			s.setAssignmentGrade(grade);
			Message m = new Message(UPDATE_SUBMISSION, s);
			sendMessage(m);
			receiveMessage();
			updateSubmissions();
			view.displayMessage("Submission updated successfully", "Updated");

			Component component = (Component) arg0.getSource();
	        JDialog dialog = (JDialog) SwingUtilities.getRoot(component);
	        // Close the dialog box
	        dialog.dispose();
		}
		
		/**
		 * Updates the list of submissions in assignment view
		 */
		private void updateSubmissions() {
			// Get all submissions related to the assignment
    			Submission sub = new Submission(0, 0, view.cv.av.c.getAssignID(), null, null, null, null, 0);
    			Message m = new Message(GET_SUBMISSION, sub);
    			sendMessage(m);
    			m = receiveMessage();
    			ArrayList<Submission> s = (ArrayList<Submission>)m.getData();
    			DefaultListModel<Submission> model = view.cv.av.model;
    			model.clear();
    			for(Submission e : s) {
    				model.addElement(e);
    			}
		}
	}
	
	/**
	  * Listener for Activate/Deactivate assingment button in assignment view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class AVActivateDeactivateListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			CourseAssignment c = view.cv.av.c;
			JButton b = (JButton)arg0.getSource();
			// Check if activating or deactivating, then switch the button to do the opposite
			if(b.getActionCommand().equals("A")) {
				c.setActive(1);
				Message m = new Message(UPDATE_ASSIGNMENT, c);
				sendMessage(m);
				receiveMessage();
				b.setText("Deactivate Assignment");
				b.setActionCommand("D");
			}
			else {
				c.setActive(0);
				Message m = new Message(UPDATE_ASSIGNMENT, c);
				sendMessage(m);
				receiveMessage();
				b.setText("Activate Assignment");
				b.setActionCommand("A");
			}
			// Update the courses so everything is updated in view
			updateCourses();
		}
	}
	
	/**
	  * Listener for Activate/Deactivate course button in course view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class CVActivateDeactivateListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			ProfessorCourse c = view.cv.course;
			JButton b = (JButton)arg0.getSource();
			// Check if activating or deactivating, then switch the button to do the opposite
			if(b.getActionCommand().equals("A")) {
				c.setActive(1);
				Message m = new Message(UPDATE_COURSE, c);
				sendMessage(m);
				receiveMessage();
				b.setText("Deactivate Course");
				b.setActionCommand("D");
			}
			else {
				c.setActive(0);
				Message m = new Message(UPDATE_COURSE, c);
				sendMessage(m);
				receiveMessage();
				b.setText("Activate Course");
				b.setActionCommand("A");
			}
			// Update the courses so everything is updated in view
			updateCourses();
		}
	}
	
	/**
	  * Listener for creating a new assignment button in course view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class CVNewAssignment implements ActionListener{
		/**
		 * Checks if the date is valid
		 * @param year the year entered
		 * @param month the month entered
		 * @param day the day entered
		 * @return false if the input is invalid. Otherwise true
		 */
		private boolean isValidDueDate(String year, String month, String day) {
			// Check if all inputs are numbers
			int yearInt = 0;
			int monthInt = 0;
			int dayInt = 0;
			try {
				yearInt = Integer.parseInt(year);
				monthInt = Integer.parseInt(month);
				dayInt = Integer.parseInt(day);
			}catch(NumberFormatException e) {
				return false;
			}
			
			// Check for valid year
			if(yearInt < 0)
				return false;
			// Check for valid month
			if(monthInt > 12 || monthInt < 1) {
				return false;
			}
			
			// Check for valid day
			if(dayInt < 1 || dayInt > 31) {
				return false;
			}

			if(monthInt == 4 || monthInt == 6 || monthInt == 9 || monthInt == 11) { // Months with 30 days
				if(dayInt > 30)
					return false;
			}
			else if(monthInt == 2) { // February.. could be a leap year or not a leap year
				boolean leap = isLeap(yearInt);
				if(leap) {
					if (dayInt > 29)
						return false;
				}
				else {
					if(dayInt > 28) {
						return false;
					}
				}
			}
			
			// Make sure the date isn't before today
			Date d = new Date();
			DateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd");
		    String timeStamp = timeFormat.format(d);
			
		    String dueDate = year + "/" + month + "/" + day;
		    if(dueDate.compareTo(timeStamp) < 0)
		    		return false;
		    
		    return true;
		}
		
		/**
		 * Checks if the specified year is a leap year
		 * Implementation from: https://www.programiz.com/java-programming/examples/leap-year
		 * @param year the year desired
		 * @return true if it's a leap year, otherwise false.
		 */
		private boolean isLeap(int year) {
			boolean leap = false;

	        if(year % 4 == 0)
	        {
	            if( year % 100 == 0)
	            {
	                // year is divisible by 400, hence the year is a leap year
	                if ( year % 400 == 0)
	                    leap = true;
	                else
	                    leap = false;
	            }
	            else
	                leap = true;
	        }
	        else
	            leap = false;
	        
	        return leap;
		}
		@Override
		public void actionPerformed(ActionEvent e) {
			String dueDateYear = view.cv.getAssignmentDueDateYear();
			String dueDateMonth = view.cv.getAssignmentDueDateMonth();
			String dueDateDay = view.cv.getAssignmentDueDateDay();
			String dueDate = null;
			view.cv.resetAssignmentDueDate();
			if(dueDateYear == null || dueDateYear.equals("") || 
				dueDateMonth == null || dueDateMonth.equals("") ||
				dueDateDay == null || dueDateDay.equals("")) {
				view.displayErrorMessage("Please Enter a due date");
				return;
			}
			if(dueDateYear.length() != 4 || dueDateMonth.length() != 2 || dueDateDay.length() != 2) {
				view.displayErrorMessage("Please enter a valid date with the following format: YYYY/MM/DD");
				return;
			}
			if(isValidDueDate(dueDateYear, dueDateMonth, dueDateDay)) {
				dueDate = dueDateYear + "/" + dueDateMonth + "/" + dueDateDay;
			}
			else {
				view.displayErrorMessage("Please enter a valid date with the following format: YYYY/MM/DD");
				return;
			}
			JFileChooser fileBrowser = new JFileChooser();
			fileBrowser.setDialogTitle("Select File");
			fileBrowser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fileBrowser.setFileFilter(new FileNameExtensionFilter("PDF or Text Files", "pdf", "txt"));
			File selectedFile = null;
			
			if(fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
				selectedFile = fileBrowser.getSelectedFile();
			else
				return;
			
			if(selectedFile == null) {
				view.displayErrorMessage("Please select a valid file");
				return;
			}

			String name = selectedFile.getName();
			String extension = name.substring(name.length()-3);
			if(!extension.equals("pdf") && !extension.equals("txt")) {
				view.displayErrorMessage("Please select a PDF of text file");
				return;
			}
			long length = selectedFile.length();
			if(length > Integer.MAX_VALUE) {
				view.displayErrorMessage("File is too big!");
				return;
			}
			
			byte[] content = new byte[(int) length]; // Converting Long to Int
			try {
				FileInputStream fis = new FileInputStream(selectedFile);
				BufferedInputStream bos = new BufferedInputStream(fis);
				bos.read(content, 0, (int)length);
				fis.close();
				bos.close();
			} catch (FileNotFoundException f) {
				f.printStackTrace();
			} catch(IOException f){
				f.printStackTrace();
			}
				
			int i = 0;	
			// Finding a valid ID for new insertion
			while(i < 99999999) {
				CourseAssignment ca = new CourseAssignment(i, view.cv.course.getCourseID(), String.valueOf(i)+"-"+selectedFile.getName(), 0, dueDate, content);
				Message m = new Message(ASSIGNMENT, ca);
				sendMessage(m);
				m = receiveMessage();
				if(m.getData() != null)
					break;
				i++;
			}
			if (i > 99999999) {
				view.displayErrorMessage("Can't add new assignment");
			}
			else {
				view.displayMessage("Assignment successfully added", "Added");
				updateCourses();
				updateAssignment();
			}
		}
		
		/**
		 * Updates the assignment list to contain new assignment
		 */
		private void updateAssignment() {
			ArrayList<CourseAssignment> assignments = view.cv.course.getAssignments();
			DefaultListModel<CourseAssignment> model = view.cv.al.model;
			model.clear();
	        for(CourseAssignment e : assignments){
	          model.addElement(e);
	        }
		}
	}
	
	/**
	  * Listener for enrolling/unenrolling button in course view
	  * @author Babafemi Adeniran and Abdulkareem Dolapo
	  * @version 1.0
	  * @since April 5, 2018
	  */
	class CVEnrollUnenrollListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			JList<StudentUser> su;
			// Figure out which list triggered it 
			if(view.cv.studentList)
				su = view.cv.sl.studentList;
			else
				su = view.cv.sc.studentList;
			
			ProfessorCourse c = view.cv.course;
			StudentUser s = su.getSelectedValue();
			su.clearSelection();
			// Find out whether enrolling or unenrolling
			if(arg0.getActionCommand().equals("E")){
				// Finding a valid ID for new insertion
				int i = 0;			
				while(i < 99999999) {
					Enrollment e = new Enrollment(i, s.getUserID(), c.getCourseID());
					Message m = new Message(ENROLLMENT, e);
					sendMessage(m);
					m = receiveMessage();
					if(m.getData() != null)
						break;
					i++;
				}
				if (i > 99999999) {
					view.displayErrorMessage("Can't enroll new student");
				}
				else {
					view.displayMessage(s.getFirstName() + " " + s.getLastName() + " has been enrolled successfully", "Enrolled");
					updateCourses();
				}
			}
			else {
				Enrollment e = new Enrollment(0, s.getUserID(), c.getCourseID());
				Message m = new Message(UNENROLLMENT, e);
				sendMessage(m);
				receiveMessage();
				view.displayMessage(s.getFirstName() + " " + s.getLastName() + " has been unenrolled", "Unenrolled");
				updateCourses();
				updateStudentList();
			}
			JButton b = (JButton)arg0.getSource();
			b.setEnabled(false);
		}
		
		/**
		 * Updates the list of students enrolled in course
		 * @author Babafemi Adeniran and Abdulkareem Dolapo
		 * @version 1.0
		 * @since April 5, 2018
		 */
		private void updateStudentList() {
			DefaultListModel<StudentUser> model;
			if(view.cv.studentList)
				model = view.cv.sl.getModel();
			else
				return;
			
			ArrayList<StudentUser> list = view.cv.course.getStudentList();
			model.clear();
	        for(StudentUser e : list){
	          model.addElement(e);
	        }
		}
	}
	
	/**
	 * Listener for send email button in email view
	 * @author Babafemi Adeniran and Abdulkareem Dolapo
	 * @version 1.0
	 * @since April 5, 2018
	 */
	class SendEmailListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			String content = view.cv.ep.getEmailcontent();
			String subject = view.cv.ep.getEmailSubject();
			if(content == null || subject == null) {
				view.displayErrorMessage("Please fill both a subject and a message");
				return;
			}
			if(content.equals("") || subject.equals("")) {
				view.displayErrorMessage("Please fill both a subject and a message");
				return;
			}
			
			Email e = new Email(content, subject, user, view.cv.course.getCourseID());
			Message m = new Message(EMAIL, e);
			sendMessage(m);
			m = receiveMessage();
			if(m.getData() == null)
				view.displayMessage("Email: " + subject + " sent", "Sent");
			else
				view.displayErrorMessage("Email not sent");
		}
	}
	
	/**
	 * Listener for searching for a student in student search view
	 * @author Babafemi Adeniran and Abdulkareem Dolapo
	 * @version 1.0
	 * @since April 5, 2018
	 */
	class StudentSearchListener implements ActionListener
	{
		@SuppressWarnings("unchecked")
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			DefaultListModel<StudentUser> model = view.cv.sc.getModel();
			// Find out whether we are searching by ID or by Last name
			if(arg0.getActionCommand().equals("ID")) {
				String idVal = view.cv.sc.getTextField();
				int id = 0;
				if(idVal.length() != 8) {
					view.displayErrorMessage("Enter a valid ID. ID needs to have 8 numbers.");
					return;
				}
				try {
					id = Integer.parseInt(idVal);
				}catch(NumberFormatException e) {
					view.displayErrorMessage("Enter a valid ID");
					return;
				}
				StudentUser req = new StudentUser("S", null, null, id, null);
				Message m = new Message(SEARCH_STUDENT_ID, req);
				sendMessage(m);
				m = receiveMessage();
				// Check if the student exists
				if(m.getData() == null) {
					view.displayErrorMessage("No student with ID: " + String.valueOf(id));
					return;
				}
					
				req = (StudentUser)m.getData();
				if(req.getType().equals("P")) {
					view.displayErrorMessage("No student with ID: " + String.valueOf(id));
					return;
				}
					
				model.clear();
				model.addElement(req);
			}
			else {
				String lastName = view.cv.sc.getTextField();
				StudentUser req = new StudentUser("S", null, lastName, 0, null);
				Message m = new Message(SEARCH_STUDENT_LN, req);
				sendMessage(m);
				m = receiveMessage();
				ArrayList<StudentUser> su = (ArrayList<StudentUser>)m.getData();
				ArrayList<StudentUser> sl = new ArrayList<>();
				for(StudentUser s : su) {
					System.out.println(s.getType());
					if(s.getType().equals("S"))
						sl.add(s);
				}
				// Check if the student exists
				if(sl.size() == 0) {
					view.displayErrorMessage("No student with Last Name: " + lastName);
					return;
				}
				
				model.clear();
			    for(StudentUser e : su){
			    		model.addElement(e);
			    }
			}
		}
	}
	
	/**
	 * Listener for a list of students
	 * @author Babafemi Adeniran and Abdulkareem Dolapo
	 * @version 1.0
	 * @since April 5, 2018
	 */
	class StudentListListener implements ListSelectionListener
	{
		@Override
	    public void valueChanged(ListSelectionEvent e) {
	      if (e.getValueIsAdjusting() == false) {
	        JList<StudentUser> list = (JList<StudentUser>)e.getSource();
	        StudentUser sel = list.getSelectedValue();
	        // Make sure student exists
	        if (sel != null) {
	        		setEnrollUnenroll(sel);
	        } 
	      }
	    }
		/**
		 * Determines whether or not the student is currently enrolled or unenrolled
		 * sets the enroll/unenroll button to enroll a student if they are unenrolled
		 * or to unenroll if they are enrolled
		 * @param sel the student selected
		 */
		private void setEnrollUnenroll(StudentUser sel) {
			boolean enroll = true;
			ProfessorCourse c = view.cv.course;
			for(StudentUser e: c.getStudentList()) {
				if(e.getUserID() == sel.getUserID()) {
					enroll = false;
					break;
				}		
			}
			JButton b = view.cv.btnEnrollUnenroll;
			if(enroll) {
				b.setText("Enroll");
				b.setActionCommand("E");
				b.setEnabled(true);
			}
			else {
				b.setText("UnEnroll");
				b.setActionCommand("U");
				b.setEnabled(true);
			}
		}
	}
	
	/**
	 * Listener for assingment list in assignment list view
	 * @author Babafemi Adeniran and Abdulkareem Dolapo
	 * @version 1.0
	 * @since April 5, 2018
	 */
	class AssignmentListListener implements ListSelectionListener
	{
		@SuppressWarnings("unchecked")
		@Override
	    public void valueChanged(ListSelectionEvent e) {
	      if (e.getValueIsAdjusting() == false) {
	        JList<CourseAssignment> list = view.cv.al.assignmentList;
	        CourseAssignment sel = list.getSelectedValue();
	        // Make sure assignment exists
	        if (sel != null) {
	        		// Get all submissions related to the assignment
	        		Submission sub = new Submission(0, 0, sel.getAssignID(), null, null, null, null, 0);
	        		Message m = new Message(GET_SUBMISSION, sub);
	        		sendMessage(m);
	        		m = receiveMessage();
	        		ArrayList<Submission> s = (ArrayList<Submission>)m.getData();
	        		view.viewAssignment(sel, s, new AVActivateDeactivateListener(), new SubmissionListListener());
	        }
	        list.clearSelection();
	      }
	    }
	}
	
	/**
	 * Listener for list of submissions in assignment view
	 * @author Babafemi Adeniran and Abdulkareem Dolapo
	 * @version 1.0
	 * @since April 5, 2018
	 */
	class SubmissionListListener implements ListSelectionListener
	{
		@SuppressWarnings("unchecked")
		@Override
	    public void valueChanged(ListSelectionEvent e) {
	      if (e.getValueIsAdjusting() == false) {
	        JList<Submission> list = view.cv.av.list;
	        Submission sel = list.getSelectedValue();
	        // Make sure assignment exists
	        if (sel != null) {
	        		view.cv.av.viewSubmission(sel, new SPUDoneListener(), new ViewSubmissionListener());
	        } 
	        list.clearSelection();
	      }
	    }
	}
	
	/**
	 * Listener for viewing the submission in the submission pop up
	 * @author Babafemi Adeniran and Abdulkareem Dolapo
	 * @version 1.0
	 * @since April 5, 2018
	 */
	class ViewSubmissionListener implements ActionListener
	{

		@Override
		public void actionPerformed(ActionEvent e) {
			Submission s = view.cv.av.spu.s;
			
			JFileChooser fileBrowser = new JFileChooser();
			fileBrowser.setDialogTitle("Select a Directory for Download");
			fileBrowser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			File selectedFile = null;
			
			if(fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
				selectedFile = fileBrowser.getSelectedFile();
			else
				return;
			
			if(selectedFile == null) {
				view.displayErrorMessage("Please select a valid directory");
				return;
			}
			// Download the file, and then try to use desktop to open default application
			String path = selectedFile.getAbsolutePath() + "/";
			File newFile = new File(path+s.getTitle());
			try{
				if(!newFile.exists())
					newFile.createNewFile();
				FileOutputStream writer = new FileOutputStream(newFile, false);
				BufferedOutputStream bos = new BufferedOutputStream(writer);
				if(s.getSubmissionFile() != null)
					bos.write(s.getSubmissionFile());
				else {
					JOptionPane.showMessageDialog(null, "No Submission File", "ERROR", JOptionPane.ERROR_MESSAGE);
					bos.close();
					return;
				}
				bos.close();
			} catch(IOException f){
				f.printStackTrace();
			}
			if (Desktop.isDesktopSupported()) {
	            try {
	                Desktop.getDesktop().open(newFile);
	            } catch (IOException ex) {
	            		view.displayErrorMessage("No application to open file. Go to destination folder and choose a way to view file");
	            }
	        }
			else {
				view.displayErrorMessage("No desktop support");
			}
		}
		
	}
	
}
