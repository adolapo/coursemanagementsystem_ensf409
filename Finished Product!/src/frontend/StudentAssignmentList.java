package frontend;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import data.CourseAssignment;

/**
 * A view for seeing the list of active assignments a student has in a certain course
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @version 1.0
 * @since April 8, 2018
 *
 */
public class StudentAssignmentList extends JPanel {
	/**
	 * The list of assignments 
	 */
	JList <CourseAssignment>assignmentList;
	/**
	 * The model for assignmentList
	 */
	DefaultListModel <CourseAssignment> model = new DefaultListModel<>();

	/**
	 * Creates a view for an assignment list based on the given list of assignments
	 * @param ca the list of assignments
	 */
	public StudentAssignmentList(ArrayList<CourseAssignment> ca) {
		setLayout(new BorderLayout(0, 0));
		assignmentList = new JList<>();
		assignmentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		assignmentList.setModel(model);
		assignmentList.setFont(new Font("Arial",Font.PLAIN,16));
		assignmentList.setFixedCellHeight(30);
		assignmentList.setFixedCellWidth(100);
		assignmentList.setBorder(new EmptyBorder(10,10, 10, 10));
		for(CourseAssignment e: ca) {
			model.addElement(e);
		}
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setViewportView(assignmentList);
		add(scrollPane);
	}
	
	/**
	 * Sets listener for assignmentList
	 * @param l the desired listener
	 */
	void setListListener(ListSelectionListener l) {
		assignmentList.addListSelectionListener(l);
	}
}
