package frontend;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
/**
 * Contains the components, fields, and methods for implementing a login screen.
 * @author Babafemi Adeniran
 * @since April 3, 2018
 * @version 1.0
 *
 */
public class LoginWindow extends JDialog {
	/**
	 * The panel that contains the input fields and labels
	 */
	private final JPanel contentPanel = new JPanel();
	/**
	 * The field that contains the user ID
	 */
	private JTextField idField;
	/**
	 * The field that contains the user's password
	 */
	private JPasswordField passwordField;
	/**
	 * The button for logging in
	 */
	private JButton login;
	/**
	 * The button for cancelling login
	 */
	private JButton cancelButton;
	/**
	 * The client trying to login 
	 */
	Client client;
	
	/**
	 * Creates a login popup that relates to the given client
	 * @param client the client trying to log in
	 */
	public LoginWindow(Client client) {
		setResizable(false);
		this.client = client;
		setBounds(100, 100, 424, 158);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(null);
		
		idField = new JTextField();
		idField.setDocument(new JTextFieldLimit(8));
		idField.setBounds(165, 18, 130, 26);
		contentPanel.add(idField);
		idField.setColumns(10);
		
		JLabel lblIDLabel = new JLabel("User ID:");
		lblIDLabel.setBounds(92, 23, 61, 16);
		contentPanel.add(lblIDLabel);
		
		JLabel lblPassLabel = new JLabel("Password:");
		lblPassLabel.setBounds(92, 59, 73, 21);
		contentPanel.add(lblPassLabel);
		
		passwordField = new JPasswordField();
		passwordField.setBounds(165, 56, 130, 26);
		contentPanel.add(passwordField);
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				login = new JButton("Login");
				login.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						String pass = getPassword();
						String userId = getID();
						if(userId.length() != 8) {
							JOptionPane.showMessageDialog(null, "Invalid Login. ID needs to be 8 characters long.", "ERROR", JOptionPane.ERROR_MESSAGE);
							return;
						}
						
						int id = 0;
						try {
							id = Integer.parseInt(userId);
						}catch(NumberFormatException f) {
							JOptionPane.showMessageDialog(null, "Invalid Login", "ERROR", JOptionPane.ERROR_MESSAGE);
							return;
						}
						login.setEnabled(false);
						if(client.login(id, pass)) {
							setVisible(false);
							client.launchView();
						}
						else {
							JOptionPane.showMessageDialog(null, "Invalid Login", "ERROR", JOptionPane.ERROR_MESSAGE);
							login.setEnabled(true);
						}
					}
				});
				login.setActionCommand("Login");
				buttonPane.add(login);
				getRootPane().setDefaultButton(login);
			}
			{
				cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent e) {
						System.exit(0);
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}
	
	/**
	 * Gets the text in the passwordField
	 * @return the text in the passwordField
	 */
	public String getPassword() {
		char [] password = passwordField.getPassword();
		String pass = String.valueOf(password);
		return pass;
	}
	
	/**
	 * Gets the text in the ID field
	 * @return the text in the ID field
	 */
	public String getID() {
		return idField.getText();
	}
}
