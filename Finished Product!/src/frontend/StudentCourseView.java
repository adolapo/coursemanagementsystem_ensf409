package frontend;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JPanel;
import javax.swing.event.ListSelectionListener;

import data.Course;
import data.CourseAssignment;

/**
 * Allows the student to view a specific course. They will be able to view assignments (actual document, grades, submission comments, and
 * upload submissions), and send an email to the professor
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @version 1.0
 * @since April 5, 2018
 */
public class StudentCourseView extends JPanel {
	/**
	 * The panel that contains the current view of the CourseView
	 */
	private JPanel content;
	/**
	 * Button for sending an email
	 */
	private JButton btnSendEmail;
	/**
	 * Button for viewing assignments
	 */
	private JButton btnAssignments;
	
	/**
	 * The course the view relates to
	 */
	Course course;
	/**
	 * The assignment listing panel
	 */
	StudentAssignmentList sal;
	/**
	 * The email panel
	 */
	EmailPane ep;
	
	/**
	 * The courently selected assignment in the assignment list
	 */
	CourseAssignment selectedAssignment;
	
	/**
	 * Creates the view for the course based on the specified parameters.
	 * Note, a listselectionlistener is passed during initialization to make the first panel the assignment list view.
	 * @param c the course the view should be related to
	 * @param l the desired list listener
	 */
	public StudentCourseView(Course c, ListSelectionListener l) {
		course = c;
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnSendEmail = new JButton("Email");
		panel.add(btnSendEmail);
		
		btnAssignments = new JButton("Assignments");
		panel.add(btnAssignments);
		
		content = new JPanel(new CardLayout());
		add(content, BorderLayout.CENTER);
		content.setLayout(new CardLayout(0, 0));
		listAssignments(l, getAssignments());
	}
	
	/**
	 * Gets the active assignments related to the course
	 * @return the active assignments related to the course
	 */
	ArrayList<CourseAssignment> getAssignments() {
		ArrayList<CourseAssignment> activeAss = new ArrayList<>();
		for(CourseAssignment e : this.course.getAssignments()) {
			if(e.isActive() == 1)
				activeAss.add(e);
		}
		return activeAss;
	}
	
	/**
	 * Sets content to be the email sending panel
	 * @param l the action listener for sending email for EmailPane
	 */
	void sendEmail(ActionListener l) {
		ep = new EmailPane();
		ep.setEmailListener(l);
		content.removeAll();
		content.add(ep, "EP");
		CardLayout c1 = (CardLayout) content.getLayout();
		c1.show(content, "EP");
		content.validate();
		content.repaint();
	}
	
	/**
	 * Sets content to be the list of assignments panel
	 * @param l the action listener for the assignment list for StudentAssignmentList
	 * @param list the list of courses to be displayed
	 */
	void listAssignments(ListSelectionListener l, ArrayList<CourseAssignment> list){
		sal = new StudentAssignmentList(list);
		sal.setListListener(l);
		content.removeAll();
		content.add(sal, "SAL");
		CardLayout c1 = (CardLayout) content.getLayout();
		c1.show(content, "SAL");
		content.validate();
		content.repaint();
	}
	
	/**
	 * Sets the listener for btnSendEmail
	 * @param l the listener desired
	 */
	void setSendEmailListener(ActionListener l) {
		btnSendEmail.addActionListener(l);
	}
	/**
	 * Sets the listener for btnAssignments
	 * @param l the listener desired
	 */
	void setAssignemntsListener(ActionListener l) {
		btnAssignments.addActionListener(l);
	}
}
