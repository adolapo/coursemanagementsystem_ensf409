package frontend;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import data.StudentUser;

/**
 * This view allows the professor to see a certain list of students
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @version 1.0
 * @since April 5, 2018
 */
public class StudentList extends JPanel {
	/**
	 * The model for studentList
	 */
	private DefaultListModel <StudentUser> model = new DefaultListModel<>();
	/**
	 * The list of students to be viewed
	 */
	JList<StudentUser> studentList;
	
	/**
	 * Creates the student list panel based on the given list
	 * @param list the list of students to be displayed
	 */
	public StudentList(ArrayList<StudentUser> list) {
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 5));
		
		JLabel lblSearch = new JLabel("Enrolled Students:");
		panel.add(lblSearch);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrollPane, BorderLayout.CENTER);
		
		studentList = new JList<>();
		studentList.setModel(model);
		for(StudentUser e : list) {
			model.addElement(e);
		}
		
		studentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		studentList.setFont(new Font("Arial",Font.PLAIN,16));
		studentList.setFixedCellHeight(30);
		studentList.setFixedCellWidth(100);
		studentList.setBorder(new EmptyBorder(10,10, 10, 10));
		scrollPane.setViewportView(studentList);
		
		add(scrollPane, BorderLayout.CENTER);
	}
	
	/**
	 * Gets the model
	 * @return the model
	 */
	DefaultListModel<StudentUser> getModel() {
		return model;
	}
	
	/**
	 * Sets the listener for studentList
	 * @param l the listener desired
	 */
	void setListListener(ListSelectionListener l) {
		studentList.addListSelectionListener(l);
	}
}
