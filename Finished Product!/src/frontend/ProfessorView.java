package frontend;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import data.CourseAssignment;
import data.ProfessorCourse;
import data.Submission;
/**
 * Implements the view for the professor GUI for the course management system.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @version 1.0
 * @since April 5, 2018
 */
public class ProfessorView extends JFrame {
	
	/**
	 * The main content view in the GUI
	 */
	private JPanel contentPane;
	/**
	 * Contains what component is occupying contentPane
	 */
	private JComponent currentContent;
	
	/**
	 * The name of the logged in user
	 */
	private JLabel lblName;
	/**
	 * The ID of the logged in user
	 */
	private JLabel lbID;
	/**
	 * The location the user is currently in in the GUI
	 */
	JLabel lblLocationLabel;
	
	/**
	 * The button to go to the home view
	 */
	private JButton btnHome;
	
	/**
	 * The home view
	 */
	private Home home;
	/**
	 * The course view
	 */
	CourseView cv;


	/**
	 * Creates the frame for the view of the professor GUI.
	 * @param home the starting home view
	 */
	public ProfessorView(Home home) {
		this.home = home;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 822, 575);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.PAGE_AXIS));
		
		JPanel panel = new JPanel();
		panel_1.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		JPanel panel_5 = new JPanel();
		FlowLayout fl_panel_5 = (FlowLayout) panel_5.getLayout();
		fl_panel_5.setAlignment(FlowLayout.LEFT);
		panel.add(panel_5);
		
		JLabel lblName_1 = new JLabel("Name: ");
		lblName_1.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		panel_5.add(lblName_1);
		
		lblName = new JLabel("John Smith");
		panel_5.add(lblName);
		lblName.setFont(new Font("Lucida Grande", Font.ITALIC, 14));
		lblName.setHorizontalAlignment(SwingConstants.LEFT);
		
		JPanel panel_6 = new JPanel();
		FlowLayout fl_panel_6 = (FlowLayout) panel_6.getLayout();
		fl_panel_6.setAlignment(FlowLayout.LEFT);
		panel.add(panel_6);
		
		JLabel lblId = new JLabel("ID: ");
		lblId.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		panel_6.add(lblId);
		
		lbID = new JLabel("12345678");
		lbID.setFont(new Font("Lucida Grande", Font.ITALIC, 14));
		panel_6.add(lbID);
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.PAGE_AXIS));
		
		JPanel panel_3 = new JPanel();
		FlowLayout fl_panel_3 = (FlowLayout) panel_3.getLayout();
		panel_2.add(panel_3);
		
		lblLocationLabel = new JLabel("Home: List of Courses");
		lblLocationLabel.setFont(new Font("Arial",Font.BOLD,18));
		panel_3.add(lblLocationLabel);
		lblLocationLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);
		btnHome = new JButton("Home");
		btnHome.setFont(new Font("Arial",Font.PLAIN,18));
		btnHome.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				getContentPane().remove(currentContent);
				currentContent = home;
				getContentPane().add("Center", currentContent);
				lblLocationLabel.setText("Home: List of Courses");
				getRootPane().validate();
				getRootPane().repaint();
			}
		});
		panel_4.add(btnHome);
		btnHome.setAlignmentX(Component.RIGHT_ALIGNMENT);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(null, "Are you sure you want to logout?", "Logout Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null);
				if(result == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
				else {
					return;
				}
			}
		});
		currentContent = home;
		getContentPane().add("Center", currentContent);
		contentPane.add(btnLogout, BorderLayout.SOUTH);
		setResizable(false);
	}

	/**
	 * Sets home to desired home
	 * @param home the home to set
	 */
	void setHome(Home home) {
		this.home = home;
	}

	/**
	 * Gets the view's home
	 * @return the home
	 */
	Home getHome() {
		return home;
	}
	
	/**
	 * Sets the text in lblName
	 * @param lblName the lblName to set
	 */
	void setLblName(String lblName) {
		this.lblName.setText(lblName);;
	}

	/**
	 * Sets the text in lbID
	 * @param lbID the lbID to set
	 */
	void setLbID(String lbID) {
		this.lbID.setText(lbID);;
	}
	
	/**
	 * Launches the course view based on the given parameters.
	 * Listselection listener included to set up default view of course view
	 * @param c the course that the view will get information from
	 * @param l the desired listener
	 */
	void launchCourseView(ProfessorCourse c, ListSelectionListener l) {
		cv = new CourseView(c, l);
		swapCurrentContent(cv);
		lblLocationLabel.setText(c.getCourseName() + ": Students");
	}
	
	/**
	 * Sets up searching for student view
	 * @param s the action listener for searching for student
	 * @param l the list listener for selecting a student
	 */
	void studentSearch(ActionListener s, ListSelectionListener l) {
		cv.btnEnrollUnenroll.setEnabled(false);
		cv.studentSearch(s, l);
		lblLocationLabel.setText(cv.course.getCourseName() + ": Student Search");
		getRootPane().validate();
		getRootPane().repaint();
	}
	
	/**
	 * Sets up sending an email view
	 * @param l the action listener for sending an email
	 */
	void email(ActionListener l) {
		cv.btnEnrollUnenroll.setEnabled(false);
		cv.sendEmail(l);
		lblLocationLabel.setText(cv.course.getCourseName() + ": Email");
		getRootPane().validate();
		getRootPane().repaint();
	}
	
	/**
	 * Sets up listing assignments view
	 * @param l the listener for selecting an assignment
	 */
	void listAssignments(ListSelectionListener l) {
		cv.btnEnrollUnenroll.setEnabled(false);
		cv.listAssignments(l);
		lblLocationLabel.setText(cv.course.getCourseName() + ": Assignments");
		getRootPane().validate();
		getRootPane().repaint();
	}
	
	/**
	 * Sets up viewing a specific assignment
	 * @param c the assignment to be viewed
	 * @param s the submissions related to the assignment
	 * @param ad the listener for activate/deactivate button
	 * @param l the listener for the submission list selection
	 */
	void viewAssignment(CourseAssignment c, ArrayList<Submission> s, ActionListener ad, ListSelectionListener l) {
		cv.viewAssignment(c, s, ad, l);
		getRootPane().validate();
		getRootPane().repaint();
	}
	
	/**
	 * Swaps the currentContent with given component
	 * @param c the component to be placed in currentContent
	 */
	void swapCurrentContent(JComponent c) {
		getContentPane().remove(currentContent);
		currentContent = c;
		getContentPane().add("Center", currentContent);
		getRootPane().validate();
		getRootPane().repaint();
	}
	
	/**
	 * Displays an error message dialog
	 * @param errorMessage the message to be displayed
	 */
	void displayErrorMessage (String errorMessage)
	{
		JOptionPane.showMessageDialog(this, errorMessage, "ERROR", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Displays a normal message dialog
	 * @param message the message to be the displayed
	 * @param title the title to be displayed
	 */
	void displayMessage(String message, String title) {
		JOptionPane.showMessageDialog(this, message, title, JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Sets the listeners for the home view
	 * @param l the listener for selecting courses
	 * @param e the listener for adding a new course
	 */
	void setHomeListeners(ListSelectionListener l, ActionListener e){
		home.setListListener(l);
		home.setAddNewCourseListener(e);
	}
	
	/**
	 * Sets the listeners for the course view
	 * @param l listener for activate/deactivate button
	 * @param l1 listener for uploadAssignment button
	 * @param l2 listener for searchStudent button
	 * @param l3 listener for enroll/unenroll button
	 * @param l4 listener for sendEmail button
	 * @param l5 listener for assignments button
	 * @param l6 listener for view students button
	 */
	void setCourseViewListeners(ActionListener l, ActionListener l1, ActionListener l2, ActionListener l3, ActionListener l4, ActionListener l5,
								ActionListener l6){
		cv.setActivateDeactivateListener(l);	
		cv.setUploadAssignmentListener(l1);	
		cv.setSearchStudentListener(l2);
		cv.setEnrollUnenrollListener(l3);
		cv.setSendEmailListener(l4); 
		cv.setAssignemntsListener(l5);
		cv.setViewStudentsListener(l6);
	}
	
}
