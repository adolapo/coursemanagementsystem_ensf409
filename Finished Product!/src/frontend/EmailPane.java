package frontend;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.MatteBorder;

/**
 * A panel for sending emails. 
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @version 1.0
 * @since April 5th, 2018
 */
public class EmailPane extends JPanel {
	/**
	 * The subject of the email
	 */
	private JTextField emailSubject;
	/**
	 * The content of the email
	 */
	private JTextArea emailcontent;
	/**
	 * The button to send the email
	 */
	JButton btnSendEmail;

	/**
	 * Create the panel for sending emails.
	 */
	public EmailPane() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		add(panel, BorderLayout.NORTH);
		
		JLabel lblSubject = new JLabel("Subject: ");
		panel.add(lblSubject);
		
		emailSubject = new JTextField();
		panel.add(emailSubject);
		emailSubject.setColumns(15);
		
		JPanel panel_1 = new JPanel();
		add(panel_1, BorderLayout.CENTER);
		panel_1.setLayout(new BorderLayout(5, 0));
		
		JPanel panel_3 = new JPanel();
		FlowLayout flowLayout_1 = (FlowLayout) panel_3.getLayout();
		flowLayout_1.setAlignment(FlowLayout.LEFT);
		panel_1.add(panel_3, BorderLayout.NORTH);
		
		JLabel lblNewLabel = new JLabel("Content: ");
		panel_3.add(lblNewLabel);
		
		emailcontent = new JTextArea();
		emailcontent.setLineWrap(true);
		emailcontent.setColumns(43);
		emailcontent.setRows(12);
		emailcontent.setEditable(true);
		JScrollPane scrollPane = new JScrollPane(emailcontent);
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		panel_1.add(scrollPane, BorderLayout.CENTER);
		
		btnSendEmail = new JButton("Send Email");
		add(btnSendEmail, BorderLayout.SOUTH);
	}
	
	/**
	 * Sets the listener for btnSendEmail
	 * @param l the desired listener
	 */
	void setEmailListener(ActionListener l) {
		btnSendEmail.addActionListener(l);
	}
	
	/**
	 * Gets the text in the emailSubject
	 * @return the emailSubject text
	 */
	String getEmailSubject() {
		return emailSubject.getText();
	}

	/**
	 * Gets the text in the emailContent
	 * @return the emailcontent text
	 */
	String getEmailcontent() {
		return emailcontent.getText();
	}
}
