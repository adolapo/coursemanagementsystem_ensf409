package frontend;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import data.*;
/**
 * Contains the data fields and methods that implement a Client for a course management client-server system.
 * @author Babafemi Adeniran and Abdulkareem Dolapo
 * @since April 3, 2018
 * @version 1.0
 */
class Client implements Requestcodes {
	/**
	 * The output handle for the socket
	 */
	private ObjectOutputStream socketOut;
	/**
	 * Socket to be used for communication
	 */
	private Socket palinSocket;
	/**
	 * The input handle for the socket
	 */
	private ObjectInputStream socketIn;
	/**
	 * The user that is to be logged in
	 */
	private User user;
	
	/**
	 * Constructs a Client object and sets up its socket based on the input parameters.
	 * @param serverName The name of the server
	 * @param portNumber The port to connect the socket to
	 */
	Client(String serverName, int portNumber) {
		try {
			palinSocket = new Socket(serverName, portNumber);
			socketOut = new ObjectOutputStream(palinSocket.getOutputStream());
			socketIn = new ObjectInputStream(palinSocket.getInputStream());
		} catch (IOException e) {
			System.err.println(e.getStackTrace());
			System.exit(0);
		}
	}
	
	/**
	 * Checks with the server if the ID and password entered are valid
	 * @param userID the ID entered
	 * @param pass the password entered
	 * @return true if it is a valid login. Otherwise, false
	 */
	boolean login(int userID, String pass) {
		// Ask server if valid login
		Login loginInfo = new Login(userID, pass);
		Message msg = new Message(LOGIN, loginInfo);
		try {
			socketOut.writeObject(msg);
			msg = (Message)socketIn.readObject();
		} catch (IOException e) {
			e.printStackTrace();
			System.exit(0);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
		if(msg.getData() == null)
			return false;
		user = (User)msg.getData();
		return true;
	}
	
	/**
	 * Launches the view based on the user type.
	 */
	void launchView() {
		if (user.getType().equals("S")) {
			// If it's a student, get a StudentUser before launching view
			Message msg = new Message(STUDENT_USER, user);
			try {
				socketOut.writeObject(msg);
				msg = (Message)socketIn.readObject();
			} catch (IOException e) {
				e.printStackTrace();
				System.exit(0);
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			}
			StudentUser sUser = (StudentUser)msg.getData();
			// Launch student view
			StudentController controller = new StudentController(socketIn, socketOut, sUser);
			controller.setUpView();
		}
		else {
			// launch prof view
			ProfessorController controller = new ProfessorController(socketIn, socketOut, user);
			controller.setUpView();
		}
	}
	
	public static void main(String[] args) throws IOException  {
		Client aClient = new Client("localhost", 9898);
		LoginWindow login = new LoginWindow(aClient);
		login.setVisible(true);
	}
}
