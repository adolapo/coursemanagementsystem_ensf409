package data;

import java.io.Serializable;

/**
 * A java data stucture that contains the data fields and methods for login.
 * @author Babafemi Adeniran
 * @since April 5, 2018
 * @version 1.0
 *
 */
public class Login implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -1345213082138949671L;
	/**
	 * The ID being used to log in
	 */
	private int loginId;
	/**
	 * The password being used to log in
	 */
	private String pass;
	/**
	 * @param loginId
	 * @param pass
	 */
	public Login(int loginId, String pass) {
		this.loginId = loginId;
		this.pass = pass;
	}
	/**
	 * @return the loginId
	 */
	public int getLoginId() {
		return loginId;
	}
	/**
	 * @param loginId the loginId to set
	 */
	public void setLoginId(int loginId) {
		this.loginId = loginId;
	}
	/**
	 * @return the pass
	 */
	public String getPass() {
		return pass;
	}
	/**
	 * @param pass the pass to set
	 */
	public void setPass(String pass) {
		this.pass = pass;
	}
}
