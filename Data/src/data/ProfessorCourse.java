/**
 * 
 */
package data;

import java.util.ArrayList;

/**
 * A java data structure that contains the fields and methods to represent a Course, accessed
 * by a professor in a course management system.
 * @author Babafemi Adeniran
 * @since April 3, 2018
 * @version 1.0
 *
 */
public class ProfessorCourse extends Course {
	/**
	 * The serial version ID for serializing ProfessorCourse
	 */
	private static final long serialVersionUID = 399113874870876666L;
	/**
	 * The list of student's in the professor's course
	 */
	private ArrayList<StudentUser> studentList;
	
	/**
	 * Creates a ProfessorCourse object based on the given parameters.
	 * @param courseID the desired courseID
	 * @param courseName the desired name of the course
	 * @param profId the Id of the prof
	 * @param isActive the desired active state of the course
	 * @param assignments the assignments in the course
	 * @param studentList the list of students
	 */
	public ProfessorCourse(int courseID, String courseName, int profId, int isActive,
			ArrayList<CourseAssignment> assignments, ArrayList<StudentUser> studentList) {
		super(courseID, courseName, profId, isActive, assignments);
		this.studentList = studentList;	
	}
	

	/**
	 * Gets the studentList
	 * @return the studentList
	 */
	public ArrayList<StudentUser> getStudentList() {
		return studentList;
	}

	/**
	 * Sets the studentList based on the desired value
	 * @param studentList the studentList to set
	 */
	public void setStudentList(ArrayList<StudentUser> studentList) {
		this.studentList = studentList;
	}
	
	public String toString() {
		String s = super.toString() + " ";
		if(isActive == 1) {
			s += "        Active";
		}
		else {
			s+= "        Inactive";
		}
		return s;
	}

}
