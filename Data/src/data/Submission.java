package data;
import java.io.Serializable;
import java.io.File;
/**
 * A java data structure that represents a Submission of a submisison file to the server.
 * @author Babafemi Adeniran
 * @since April 3, 2018
 * @version 1.0
 *
 */
public class Submission implements Serializable{
	/**
	 * The serial version ID for serializing Submission
	 */
	private static final long serialVersionUID = -3089083645967617399L;
	/**
	 * ID for the submission
	 */
	private int submissionID;
	/**
	 * The ID of the student submitting the file
	 */
	private int studentID;
	/**
	 * The ID of the assigment the submission corresponds to
	 */
	private int assignmentID;
	/**
	 * 
	 */
	private int assignmentGrade;
	/**
	 * The File that was submitted
	 */
	private byte [] submissionFile;
	/**
	 * The title of the submission
	 */
	private String title;
	/**
	 * The comment in the submission
	 */
	private String comment;
	/**
	 * The time of the submission
	 */
	private String timeStamp;
	
	/**
	 * Constructs a Submission object based on the given parameters
	 * @param studentID the ID of the student
	 * @param assignmentID the ID of the assignment
	 * @param submissionFile the file to be submitted
	 * @param title the title of the submission
	 * @param comment the comment of the student
	 * @param timeStamp the time the submission was submitted
	 */
	public Submission(int submissionID, int studentID, int assignmentID, byte [] submissionFile, String title, String comment,
			String timeStamp, int assignmentGrade) {
		this.setSubmissionID(submissionID);
		this.studentID = studentID;
		this.assignmentID = assignmentID;
		this.submissionFile = submissionFile;
		this.title = title;
		this.comment = comment;
		this.timeStamp = timeStamp;
		this.assignmentGrade = assignmentGrade;
	}

	/**
	 * Gets the studentID
	 * @return the studentID
	 */
	public int getUserID() {
		return studentID;
	}

	/**
	 * Sets the student ID to the desired value
	 * @param studentID the studentID to set
	 */
	public void setUserID(int studentID) {
		this.studentID = studentID;
	}

	/**
	 * Gets the assignmentID
	 * @return the assignmentID
	 */
	public int getAssignmentID() {
		return assignmentID;
	}

	/**
	 * Sets the assignment ID based on the desired value
	 * @param assignmentID the assignmentID to set
	 */
	public void setAssignmentID(int assignmentID) {
		this.assignmentID = assignmentID;
	}

	/**
	 * Gets the submission file
	 * @return the submissionFile
	 */
	public byte [] getSubmissionFile() {
		return submissionFile;
	}

	/**
	 * Sets the submisison file based on th edesired file
	 * @param submissionFile the submissionFile to set
	 */
	public void setSubmissionFile(byte [] submissionFile) {
		this.submissionFile = submissionFile;
	}

	/**
	 * Gets the title of the submission file
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * Sets the title of the submisison file based on the given parameter
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

	/**
	 * @return the timeStamp
	 */
	public String getTimeStamp() {
		return timeStamp;
	}

	/**
	 * @param timeStamp the timeStamp to set
	 */
	public void setTimeStamp(String timeStamp) {
		this.timeStamp = timeStamp;
	}

	public int getSubmissionID() {
		return submissionID;
	}

	public void setSubmissionID(int submissionID) {
		this.submissionID = submissionID;
	}

	public int getAssignmentGrade() {
		return assignmentGrade;
	}

	public void setAssignmentGrade(int assignmentGrade) {
		this.assignmentGrade = assignmentGrade;
	}
	
	public String toString() {
		String s = "Title: " + title + "   |   Student ID: " + studentID + "   |   Submitted: " + timeStamp;
		return s;
	}
	
}
