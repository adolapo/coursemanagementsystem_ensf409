/**
 *
 */
package data;
import java.io.Serializable;

/**
 * A java data structure that represents a user for a course management system.
 * @author Babafemi Adeniran
 * @since April 3, 2018
 * @version 1.0
 *
 */
public class User implements Serializable{

	/**
	 * The serial version ID for serializing User
	 */
	private static final long serialVersionUID = 2985209961980697415L;
	/**
	 * The type of user. Could be 'S' or 'P'
	 */
	protected String type;
	/**
	 * The first name of the user
	 */
	protected String firstName;
	/**
	 * The last name of the user
	 */
	protected String lastName;
	/**
	 * The ID of the user
	 */
	protected int userID;

	/**
	 * Constructs a User object based on the given parameters
	 * @param type the type of the user
	 * @param firstName the first name of the user
	 * @param lastName the last name of the user
	 * @param userID the user's ID
	 */
	public User(String type, String firstName, String lastName, int userID) {
		this.type = type;
		this.firstName = firstName;
		this.lastName = lastName;
		this.userID = userID;
	}

	/**
	 * Gets the type
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * Sets the type based on the desired value
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}

	/**
	 * Gets the firstName
	 * @return the firstName
	 */
	public String getFirstName() {
		return firstName;
	}

	/**
	 * Sets the firstName based on the desired value
	 * @param firstName the firstName to set
	 */
	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	/**
	 * Gets the lastName
	 * @return the lastName
	 */
	public String getLastName() {
		return lastName;
	}

	/**
	 * Sets the lastName based on the desired value
	 * @param lastName the lastName to set
	 */
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	/**
	 * Gets the userID
	 * @return the userID
	 */
	public int getUserID() {
		return userID;
	}

	/**
	 * Sets the userID based on the desired value
	 * @param userID the userID to set
	 */
	public void setUserID(int userID) {
		this.userID = userID;
	}
}
