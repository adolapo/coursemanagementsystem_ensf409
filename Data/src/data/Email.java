/**
 * 
 */
package data;

import java.io.Serializable;

/**
 * A java data structure that contains the fields and methods to represent an Email.
 * @author Babafemi Adeniran
 * @since April 3, 2018
 * @version 1.0
 *
 */
public class Email implements Serializable {

	/**
	 * The serial version ID for serializing Email
	 */
	private static final long serialVersionUID = -8764791405920878891L;
	/**
	 * The content of the email
	 */
	private String content;
	/**
	 * The subject of the email
	 */
	private String subject;
	/**
	 * The user that sent the email
	 */
	private User sender;
	/**
	 * The ID of the course from which the email relates to
	 */
	private int courseID;
	
	/**
	 * Constructs an email object based on the given parameters
	 * @param content the content of the email
	 * @param subject the subject of the email
	 * @param sender the sender of the email
	 * @param courseID the id of the course 
	 */
	public Email(String content, String subject, User sender, int courseID) {
		this.content = content;
		this.subject = subject;
		this.sender = sender;
		this.courseID = courseID;
	}

	/**
	 * Gets the content
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * Sets the content based on the desired value
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * Gets the subject
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}

	/**
	 * Sets the subject based on the desired value
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}

	/**
	 * Gets the sender
	 * @return the sender
	 */
	public User getSender() {
		return sender;
	}

	/**
	 * Sets the sender based on the desired value
	 * @param sender the sender to set
	 */
	public void setSender(User sender) {
		this.sender = sender;
	}

	/**
	 * Gets the courseID
	 * @return the courseID
	 */
	public int getCourseID() {
		return courseID;
	}

	/**
	 * Sets the courseID based on the desired value
	 * @param courseID the courseID to set
	 */
	public void setCourseID(int courseID) {
		this.courseID = courseID;
	}
}
