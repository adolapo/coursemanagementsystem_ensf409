package data;
import java.io.Serializable;
/**
 * A java data structure that represents a message that will be sent between the client
 * and server for a course management system.
 * @author Babafemi Adeniran
 * @since April 3, 2018
 * @verison 1.0
 *
 */
public class Message implements Serializable {
	/**
	 * The serial version ID for serializing Message
	 */
	private static final long serialVersionUID = 7865571052261886703L;
	/**
	 * Determines the type of message to be obtained
	 */
	private int requestCode;
	/**
	 * The data held in the message
	 */
	private Object data;
	
	/**
	 * Constructs a message object based on the given parameters
	 * @param requestCode the desired request code
	 * @param data the desired data
	 */
	public Message(int requestCode, Object data) {
		this.requestCode = requestCode;
		this.data = data;
	}
	
	/**
	 * Gets the request code in message
	 * @return requestCode
	 */
	public int getRequestCode() {
		return requestCode;
	}
	
	/**
	 * Sets the requestCode to the desired value
	 * @param requestCode desired request code
	 */
	public void setRequestCode(int requestCode) {
		this.requestCode = requestCode;
	}
	
	/**
	 * Gets the data in the message
	 * @return data
	 */
	public Object getData() {
		return data;
	}
	
	/**
	 * Sets the data to the desired data
	 * @param data desired data
	 */
	public void setData(Object data) {
		this.data = data;
	}
	
}
