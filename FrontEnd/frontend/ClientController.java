/**
 * 
 */
package frontend;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

import data.*;

/**
 * Contains the data fields and methods to implement the controller for a client in a course management system.
 * @author Babafemi Adeniran
 * @since April 3, 2018
 * @version 1.0
 */
abstract class ClientController implements Requestcodes {
	/**
	 * The input handle for the socket
	 */
	protected ObjectInputStream socketIn;
	/**
	 * The output handle for the socket
	 */
	protected ObjectOutputStream socketOut;

	/**
	 * Creates a controller and sets the input and output object streams to the desired parameters
	 * @param socketIn the input stream desired
	 * @param socketOut the output stream desired
	 */
	ClientController(ObjectInputStream socketIn, ObjectOutputStream socketOut) {
		this.socketIn = socketIn;
		this.socketOut = socketOut;
	}
	
	/**
	 * Sends a Message to the server
	 * @param m the message to be sent
	 */
	protected void sendMessage(Message m){
		try {
			socketOut.writeObject(m);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
	}
	
	/**
	 * Receives a Message from the server
	 * @return the message received
	 */
	protected Message receiveMessage(){
		Message m = null;
		try {
			m = (Message)socketIn.readObject();
		} catch (ClassNotFoundException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			System.exit(0);
		}
		return m;
	}
	
	/**
	 * Sets up the view
	 */
	protected abstract void setUpView();
}
