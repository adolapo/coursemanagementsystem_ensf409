package frontend;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.swing.JFileChooser;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.filechooser.FileNameExtensionFilter;

import data.Course;
import data.CourseAssignment;
import data.Email;
import data.Message;
import data.StudentUser;
import data.Submission;
/**
 * A class that controls events from a view and communicates with server.
 * @author Babafemi Adeniran
 * @version 1.0
 * @since April 8, 2018
 *
 */
class StudentController extends ClientController {
	/**
	 * The GUI the controller receives events from
	 */
	private StudentView view;
	/**
	 * The user that has logged in.
	 */
	private StudentUser user;
	
	/**
	 * Creates a controller based on the given parameters. 
	 * @param socketIn the input stream for the client
	 * @param socketOut the output stream for the client
	 * @param user the user that has logged in
	 */
	public StudentController(ObjectInputStream socketIn, ObjectOutputStream socketOut, StudentUser user) {
		super(socketIn, socketOut);
		this.user = user;
	}


	@Override
	protected void setUpView() {
		String name = user.getFirstName() + " "+ user.getLastName();
		String id = String.format("%08d", user.getUserID());
		ArrayList<Course> courses = getCourses();
		StudentHome home = new StudentHome(courses);
		
		// Launches GUI when everything has finished setting up
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					view = new StudentView(home);
					view.setLblName(name);
					view.setLbID(id);
					view.setListeners(new HomeButtonListener());
					view.setHomeListeners(new HomeListListener());
					view.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});	
	}	
	
	/**
	 * Gets the active courses the user has
	 * @return the list of active courses the user has
	 */
	@SuppressWarnings("unchecked")
	 ArrayList<Course> getCourses() {
		Message m = new Message(STUDENT_USER, user);
		sendMessage(m);
		m = (Message)receiveMessage();
		StudentUser userN = (StudentUser)m.getData();
		user = userN;
		ArrayList<Course> courses = user.getCourseList();
		ArrayList<Course> activeCourses = new ArrayList<>();
		for(Course e : courses) {
			if(e.isActive() == 1)
				activeCourses.add(e);
		}
		return activeCourses;
	}
	
	/**
	 * Updates the courses the user has. Tries to also update the current course. 
	 * @return if the user is not enrolled in a course anymore, return false. Otherwise, return true
	 */
	boolean updateCourse() {
		ArrayList<Course> courses = getCourses();
		for(Course e : courses) {
			if(e.getCourseID() == view.scv.course.getCourseID()) {
				view.scv.course = e;
				return true;
			}
		}
		return false;
	}
	
	/**************************************************************************/
	/**************************************************************************/
	/**********************************LISTENERS*******************************/
	/**************************************************************************/
	/**************************************************************************/
	
	/**
	  * Listener for the list of courses in student home view
	  * @author Babafemi Adeniran
	  * @version 1.0
	  * @since April 8, 2018
	  */
	class HomeListListener implements ListSelectionListener
	{
		@Override
	    public void valueChanged(ListSelectionEvent e) {
	      if (e.getValueIsAdjusting() == false) {
	        JList<Course> list = view.getHome().list;
	        Course sel = list.getSelectedValue();
	        // Make sure course exists
	        if (sel != null) {
	        		view.launchCourseView(sel, new AssignmentListListener());
	        		view.setCourseViewListeners(new SCVEmailListener(), new ListAssignmentsListener());
	        }  
	        list.clearSelection();
	      }
	    }
	}
	
	/**
	  * Listener for the home button in student view
	  * @author Babafemi Adeniran
	  * @version 1.0
	  * @since April 8, 2018
	  */
	class HomeButtonListener implements ActionListener
	{
		@Override
	    public void actionPerformed(ActionEvent e) {
	      ArrayList<Course> courses = getCourses();
	      view.getHome().updateCourses(courses);
	      view.launchHome();
	    }
	}
	
	/**
	  * Listener for send email button in student course view
	  * @author Babafemi Adeniran
	  * @version 1.0
	  * @since April 8, 2018
	  */
	class SCVEmailListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			view.email(new SendEmailListener());
		}
	}
	
	/**
	 * Listener for send email button in email view
	 * @author Babafemi Adeniran
	 * @version 1.0
	 * @since April 8, 2018
	 */
	class SendEmailListener implements ActionListener
	{
		@Override
		public void actionPerformed (ActionEvent arg0)
		{
			String content = view.scv.ep.getEmailcontent();
			String subject = view.scv.ep.getEmailSubject();
			if(content == null || subject == null) {
				view.displayErrorMessage("Please fill both a subject and a message");
				return;
			}
			if(content.equals("") || subject.equals("")) {
				view.displayErrorMessage("Please fill both a subject and a message");
				return;
			}
			
			Email e = new Email(content, subject, user, view.scv.course.getCourseID());
			Message m = new Message(EMAIL, e);
			sendMessage(m);
			m = receiveMessage();
			if(m.getData() == null)
				view.displayMessage("Email: " + subject + " sent", "Sent");
			else
				view.displayErrorMessage("Email not sent");
		}
	}
	
	/**
	  * Listener for assignments button in student course view
	  * @author Babafemi Adeniran
	  * @version 1.0
	  * @since April 8, 2018
	  */
	class ListAssignmentsListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			if(!updateCourse()) {
				view.displayErrorMessage("Can't load course");
				view.launchHome();
				return;
			}
			ArrayList<CourseAssignment> assignments = view.scv.getAssignments();
			view.scv.listAssignments(new AssignmentListListener(), assignments);
		}
	}
	
	/**
	  * Listener for list of assignments in student assignment list view
	  * @author Babafemi Adeniran
	  * @version 1.0
	  * @since April 8, 2018
	  */
	class AssignmentListListener implements ListSelectionListener
	{
		@SuppressWarnings("unchecked")
		@Override
	    public void valueChanged(ListSelectionEvent e) {
	      if (e.getValueIsAdjusting() == false) {
	        JList<CourseAssignment> list = view.scv.sal.assignmentList;
	        CourseAssignment sel = list.getSelectedValue();
	        // Make sure assignment exists
	        if (sel != null) {
	        		// Check to see if they have submitted before
	        		Submission sub = new Submission(0, user.getUserID(), sel.getAssignID(), null, null, null, null, 0);
	        		Message m = new Message(VERIFY_SUBMISSION, sub);
	        		sendMessage(m);
	        		m = receiveMessage();
	        		
	        		ArrayList<Submission> sl = (ArrayList<Submission>)m.getData();
	        		int grade = 0;
	        		if(sl.size() == 0) // If they haven't submitted, set grade to -1
	        			grade = -1;
	        		else {
	        			sub = sl.get(0);
	        			grade = sub.getAssignmentGrade();
	        		}
	        		// Save the selected assignment
	        		view.scv.selectedAssignment = sel;
	        		
	        		// Generate popup
	        		StudentAssignmentPopUp sapu = new StudentAssignmentPopUp(grade, sub.getComment());
	        		sapu.setTitle(view.scv.selectedAssignment.getTitle());
	        		sapu.setUploadListener(new SCVNewSubmisison());
	        		sapu.setDownloadListener(new ViewAssignmentListener());
	        		sapu.setVisible(true);
	        }
	        list.clearSelection();
	      }
	    }
	}
	
	/**
	  * Listener for uploading assignment in student assignment popup
	  * @author Babafemi Adeniran
	  * @version 1.0
	  * @since April 8, 2018
	  */
	class SCVNewSubmisison implements ActionListener{
		@Override
		public void actionPerformed(ActionEvent e) {
			JFileChooser fileBrowser = new JFileChooser();
			fileBrowser.setDialogTitle("Select File");
			fileBrowser.setFileSelectionMode(JFileChooser.FILES_ONLY);
			fileBrowser.setFileFilter(new FileNameExtensionFilter("PDF or Text Files", "pdf", "txt"));
			File selectedFile = null;
			
			if(fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
				selectedFile = fileBrowser.getSelectedFile();
			else
				return;
			
			if(selectedFile == null) {
				view.displayErrorMessage("Please select a valid file");
				return;
			}
			
			String name = selectedFile.getName();
			String extension = name.substring(name.length()-3);
			if(!extension.equals("pdf") && !extension.equals("txt")) {
				view.displayErrorMessage("Please select a PDF of text file");
				return;
			}
			// Make sure file isn't too big
			long length = selectedFile.length();
			if(length > Integer.MAX_VALUE) {
				view.displayErrorMessage("File is too big!");
				return;
			}
			
			byte[] content = new byte[(int) length]; // Converting Long to Int
			try {
				FileInputStream fis = new FileInputStream(selectedFile);
				BufferedInputStream bos = new BufferedInputStream(fis);
				bos.read(content, 0, (int)length);
				fis.close();
				bos.close();
			} catch (FileNotFoundException f) {
				f.printStackTrace();
			} catch(IOException f){
				f.printStackTrace();
			}
			// Check to see if they have submitted before
			Submission sub = new Submission(0, user.getUserID(), view.scv.selectedAssignment.getAssignID(), null, null, null, null, 0);
			Message m = new Message(VERIFY_SUBMISSION, sub);
    			sendMessage(m);
    			m = receiveMessage();
    			ArrayList<Submission> sl = (ArrayList<Submission>)m.getData();
    			Date d = new Date();
    			DateFormat timeFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm a");
    		    String timeStamp = timeFormat.format(d);
    		    
    		    // If they haven't submitted before create a new submission
    			if(sl.size() == 0) {
				int i = 0;			
				while(i < 99999999) {
					Submission s = new Submission(i, user.getUserID(),view.scv.selectedAssignment.getAssignID(), content, 
							String.valueOf(i)+"-"+String.valueOf(user.getUserID())+"-"+selectedFile.getName(), "", timeStamp, -1);
					m = new Message(SUBMISSION, s);
					sendMessage(m);
					m = receiveMessage();
					if(m.getData() != null)
						break;
					i++;
				}
				if (i > 99999999) {
					view.displayErrorMessage("Can't upload submisison");
				}
				else {
					view.displayMessage("Submission successfully uploaded", "Uploaded");
				}
    			}
    			else { // If they have submitted before, just update their current submission
        			sub = sl.get(0);
    				Submission s = new Submission(sub.getSubmissionID(), user.getUserID(),view.scv.selectedAssignment.getAssignID(), content, 
							String.valueOf(sub.getSubmissionID())+"-"+String.valueOf(user.getUserID())+"-"+selectedFile.getName(), "", timeStamp, -1);
				m = new Message(UPDATE_SUBMISSION, s);
				sendMessage(m);
				m = receiveMessage();
				view.displayMessage("Submission successfully updated", "Updated");
    			}
		}
	}
	
	/**
	  * Listener for viewing an assignment in student assignment pop up
	  * @author Babafemi Adeniran
	  * @version 1.0
	  * @since April 8, 2018
	  */
	class ViewAssignmentListener implements ActionListener
	{
		@Override
		public void actionPerformed(ActionEvent e) {
			CourseAssignment ca = view.scv.selectedAssignment;
			
			JFileChooser fileBrowser = new JFileChooser();
			fileBrowser.setDialogTitle("Select a Directory for Download");
			fileBrowser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
			File selectedFile = null;
			
			if(fileBrowser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION)
				selectedFile = fileBrowser.getSelectedFile();
			else
				return;
			
			if(selectedFile == null) {
				view.displayErrorMessage("Please select a valid directory");
				return;
			}
			if(selectedFile.isFile()) {
				view.displayErrorMessage("Please select a directory");
				return;
			}
			String path = selectedFile.getAbsolutePath() + "/";
			File newFile = new File(path + ca.getAssignID() +"-" + ca.getTitle());
			// Download the file
			try{
				if(!newFile.exists())
					newFile.createNewFile();
				FileOutputStream writer = new FileOutputStream(newFile, false);
				BufferedOutputStream bos = new BufferedOutputStream(writer);
				if(ca.getAssignmentFile() != null)
					bos.write(ca.getAssignmentFile());
				else {
					JOptionPane.showMessageDialog(null, "No Submission File", "ERROR", JOptionPane.ERROR_MESSAGE);
					bos.close();
					writer.close();
					return;
				}
				bos.close();
				writer.close();
			} catch(IOException f){
				f.printStackTrace();
			}
			// Try to open the file
			if (Desktop.isDesktopSupported()) {
	            try {
	                Desktop.getDesktop().open(newFile);
	            } catch (IOException ex) {
	            		view.displayErrorMessage("No application to open file. Go to destination folder and choose a way to view file");
	                System.out.println("Good try");
	            } catch(Exception f) {
	            		view.displayErrorMessage("Something went wrong!\n" + f.getMessage());
	            }
	        }
			else {
				view.displayErrorMessage("No desktop support");
			}
		}
		
	}

}
