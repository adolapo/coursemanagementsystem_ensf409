package frontend;

import java.awt.BorderLayout;
import java.awt.CardLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;
import javax.swing.event.ListSelectionListener;

import data.CourseAssignment;
import data.ProfessorCourse;
import data.StudentUser;
import data.Submission;
/**
 * Allows the professor to view a specific course. They will be able to activate/deactive the course, view students enrolled in the course,
 * add a new assignment, search for students by ID and last name, enroll a new student, view submissions for an assignment, and grade and
 * make comments on submissions.
 * @author Babafemi Adeniran
 * @version 1.0
 * @since April 5, 2018
 */
public class CourseView extends JPanel {
	/**
	 * The panel that contains the current view of the CourseView
	 */
	private JPanel content;	
	/**
	 * Field where year of due date for assignment is placed
	 */
	private JTextField assignmentDueDateYear;
	/**
	 * Field where month of due date for assignment is placed
	 */
	private JTextField assignmentDueDateMonth;
	/**
	 * Field where day of due date for assignment is placed
	 */
	private JTextField assignmentDueDateDay;
	/**
	 * Button for activating/deactivating a course
	 */
	private JButton btnActivateDeactivate;
	/**
	 * Button for uploading a new assingment
	 */
	private JButton btnUploadAssignment;
	/**
	 * Button for searching for a student
	 */
	private JButton btnSeartchStudent;
	/**
	 * Button for sending an email
	 */
	private JButton btnSendEmail;
	/**
	 * Button for viewing assignments
	 */
	private JButton btnAssignments;
	/**
	 * Button for viewing students
	 */
	private JButton btnViewStudentsButton;
	/**
	 * Button for enrolling/unenrolling a selected student
	 */
	JButton btnEnrollUnenroll;
	
	/**
	 * The course the view relates to
	 */
	ProfessorCourse course;
	/**
	 * The student searching panel
	 */
	StudentSearch sc;
	/**
	 * The student listing panel
	 */
	StudentList sl;
	/**
	 * The email panel
	 */
	EmailPane ep;
	/**
	 * The assignment listing panel
	 */
	AssignmentList al;
	/**
	 * The assignment viewing panel
	 */
	AssignmentView av;	
	
	/**
	 * Indicates whether the current studentList is from the viewing students or searching students
	 */
	boolean studentList;

	/**
	 * Creates the view for the course based on the specified parameters.
	 * Note, a listselectionlistener is passed durng initialization to make the first panel the student list view.
	 * @param c the course the view should be related to
	 * @param sl the desired list listener
	 */
	public CourseView(ProfessorCourse c, ListSelectionListener sl) {
		course = c;
		setLayout(new BorderLayout(0, 0));
		
		JPanel buttonPanel = new JPanel();
		add(buttonPanel, BorderLayout.NORTH);
		buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
		
		btnViewStudentsButton = new JButton("View Students");
		buttonPanel.add(btnViewStudentsButton);
		
		btnSeartchStudent = new JButton("Search Students");
		buttonPanel.add(btnSeartchStudent);
		
		btnEnrollUnenroll = new JButton("Enroll");
		btnEnrollUnenroll.setEnabled(false);
		buttonPanel.add(btnEnrollUnenroll);
		
		btnSendEmail = new JButton("Email");
		buttonPanel.add(btnSendEmail);
		
		btnAssignments = new JButton("Assignments");
		buttonPanel.add(btnAssignments);
		
		btnActivateDeactivate = new JButton("Null");
		if(c.isActive() == 1) {
			btnActivateDeactivate.setText("Deactivate Course");
			btnActivateDeactivate.setActionCommand("D");
		}
		else {
			btnActivateDeactivate.setText("Activate Course");
			btnActivateDeactivate.setActionCommand("A");
		}
		buttonPanel.add(btnActivateDeactivate);
		
		content = new JPanel(new CardLayout());
		add(content, BorderLayout.CENTER);
		content.setLayout(new CardLayout(0, 0));
		
		JPanel uploadAssignPanel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) uploadAssignPanel.getLayout();
		flowLayout.setHgap(4);
		add(uploadAssignPanel, BorderLayout.SOUTH);
		
		JLabel lblDueDateLabel = new JLabel("Due Date (YYYY/MM/DD):");
		uploadAssignPanel.add(lblDueDateLabel);
		
		assignmentDueDateYear = new JTextField();
		assignmentDueDateYear.setDocument(new JTextFieldLimit(4));
		assignmentDueDateYear.setEnabled(false);
		uploadAssignPanel.add(assignmentDueDateYear);
		assignmentDueDateYear.setColumns(4);
		
		JLabel label = new JLabel("/");
		uploadAssignPanel.add(label);
		
		assignmentDueDateMonth = new JTextField();
		assignmentDueDateMonth.setDocument(new JTextFieldLimit(2));
		assignmentDueDateMonth.setEnabled(false);
		uploadAssignPanel.add(assignmentDueDateMonth);
		assignmentDueDateMonth.setColumns(2);
		
		JLabel label_1 = new JLabel("/");
		uploadAssignPanel.add(label_1);
		
		assignmentDueDateDay = new JTextField();
		assignmentDueDateDay.setDocument(new JTextFieldLimit(2));
		assignmentDueDateDay.setEnabled(false);
		uploadAssignPanel.add(assignmentDueDateDay);
		assignmentDueDateDay.setColumns(2);
		
		btnUploadAssignment = new JButton("Upload Assignment");
		btnUploadAssignment.setEnabled(false);
		uploadAssignPanel.add(btnUploadAssignment);
		
		// Make first view the student list
		studentList(sl, this.course.getStudentList());
	}
	
	/**
	 * Gets the text in assignmentDueDateYear
	 * @return the assignmentDueDateYear text
	 */
	String getAssignmentDueDateYear() {
		return assignmentDueDateYear.getText();
	}
	
	/**
	 * Gets the text in assignmentDueDateMonth
	 * @return the assignmentDueDateMonth text
	 */
	String getAssignmentDueDateMonth() {
		return assignmentDueDateMonth.getText();
	}
	
	/**
	 * Gets the text in assignmentDueDate
	 * @return the assignmentDueDate text
	 */
	String getAssignmentDueDateDay() {
		return assignmentDueDateDay.getText();
	}
	
	/**
	 * Resets the dueDate to be empty
	 */
	void resetAssignmentDueDate() {
		assignmentDueDateYear.setText("");
		assignmentDueDateMonth.setText("");
		assignmentDueDateDay.setText("");
	}
	
	/**
	 * Sets the listener for btnActivateDeactivate
	 * @param l the listener desired
	 */
	void setActivateDeactivateListener(ActionListener l) {
		btnActivateDeactivate.addActionListener(l);
	}
	
	/**
	 * Sets the listener for btnUploadAssignment
	 * @param l the listener desired
	 */
	void setUploadAssignmentListener(ActionListener l) {
		btnUploadAssignment.addActionListener(l);
	}
	
	/**
	 * Sets the listener for btnSeartchStudent
	 * @param l the listener desired
	 */
	void setSearchStudentListener(ActionListener l) {
		btnSeartchStudent.addActionListener(l);
	}
	
	/**
	 * Sets the listener for btnEnrollUnenroll
	 * @param l the listener desired
	 */
	void setEnrollUnenrollListener(ActionListener l) {
		btnEnrollUnenroll.addActionListener(l);
	}
	
	/**
	 * Sets the listener for btnSendEmail
	 * @param l the listener desired
	 */
	void setSendEmailListener(ActionListener l) {
		btnSendEmail.addActionListener(l);
	}
	
	/**
	 * Sets the listener for btnAssignments
	 * @param l the listener desired
	 */
	void setAssignemntsListener(ActionListener l) {
		btnAssignments.addActionListener(l);
	}
	
	/**
	 * Sets the listener for btnViewStudentsButton
	 * @param l the listener desired
	 */
	void setViewStudentsListener(ActionListener l) {
		btnViewStudentsButton.addActionListener(l);
	}
	
	/**
	 * Sets content to be the student searching panel
	 * @param s the action listener for search buttons in StudentSearch
	 * @param l the list listener for the list in StudentSearch
	 */
	void studentSearch(ActionListener s, ListSelectionListener l) {
		studentList = false;
		btnUploadAssignment.setEnabled(false);
		assignmentDueDateYear.setEnabled(false);
		assignmentDueDateMonth.setEnabled(false);
		assignmentDueDateDay.setEnabled(false);
		sc = new StudentSearch();
		sc.setSearchById(s);
		sc.setSearchByLastName(s);
		sc.setListListener(l);
		content.removeAll();
		content.add(sc, "SC");
		CardLayout c1 = (CardLayout) content.getLayout();
		c1.show(content, "SC");
		content.validate();
		content.repaint();
	}
	
	/**
	 * Sets content to be the student listing panel
	 * @param l the action listener for for the list in StudentList
	 * @param list the list of students to be displayed in StudentList
	 */
	void studentList(ListSelectionListener l, ArrayList<StudentUser> list) {
		studentList = true;
		btnUploadAssignment.setEnabled(false);
		assignmentDueDateYear.setEnabled(false);
		assignmentDueDateMonth.setEnabled(false);
		assignmentDueDateDay.setEnabled(false);
		sl = new StudentList(list);
		sl.setListListener(l);
		content.removeAll();
		content.add(sl, "SL");
		CardLayout c1 = (CardLayout) content.getLayout();
		c1.show(content, "SL");
		content.validate();
		content.repaint();
	}
	
	/**
	 * Sets content to be the email sending panel
	 * @param l the action listener for sending email for EmailPane
	 */
	void sendEmail(ActionListener l) {
		studentList = false;
		btnUploadAssignment.setEnabled(false);
		assignmentDueDateYear.setEnabled(false);
		assignmentDueDateMonth.setEnabled(false);
		assignmentDueDateDay.setEnabled(false);
		ep = new EmailPane();
		ep.setEmailListener(l);
		content.removeAll();
		content.add(ep, "EP");
		CardLayout c1 = (CardLayout) content.getLayout();
		c1.show(content, "EP");
		content.validate();
		content.repaint();
	}
	
	/**
	 * Sets content to be the list of assignments panel
	 * @param l the action listener for the assignment list for AssignmentList
	 */
	void listAssignments(ListSelectionListener l){
		studentList = false;
		btnUploadAssignment.setEnabled(true);
		assignmentDueDateYear.setEnabled(true);
		assignmentDueDateMonth.setEnabled(true);
		assignmentDueDateDay.setEnabled(true);
		al = new AssignmentList(course.getAssignments());
		al.setListListener(l);
		content.removeAll();
		content.add(al, "AL");
		CardLayout c1 = (CardLayout) content.getLayout();
		c1.show(content, "AL");
		content.validate();
		content.repaint();
	}
	
	/**
	 * Sets content to be the view of the assignment desired
	 * @param c the assignment desired
	 * @param s the submissions relating to the assignment
	 * @param ad the listener for the activate/deactivate button for AssignmentView
	 * @param l the list listener for the submission list in AssignmentView
	 */
	void viewAssignment(CourseAssignment c, ArrayList<Submission> s, ActionListener ad, ListSelectionListener l) {
		studentList = false;
		btnUploadAssignment.setEnabled(false);
		assignmentDueDateYear.setEnabled(false);
		assignmentDueDateMonth.setEnabled(false);
		assignmentDueDateDay.setEnabled(false);
		av = new AssignmentView(c, s);
		av.setActivateDeactivate(ad);
		av.setListListener(l);
		content.removeAll();
		content.add(av, "AV");
		CardLayout c1 = (CardLayout) content.getLayout();
		c1.show(content, "AV");
		content.validate();
		content.repaint();
	}
}
