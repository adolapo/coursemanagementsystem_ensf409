package frontend;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionListener;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import data.StudentUser;
/**
 * Allows the professor to search and view students.
 * @author Babafemi Adeniran
 * @verison 1.0
 * @since April 5, 2018
 *
 */
public class StudentSearch extends JPanel {
	/**
	 * The field that the search parameter is entered into
	 */
	private JTextField searchParameter;
	/**
	 * The model for studentList
	 */
	private DefaultListModel <StudentUser> model = new DefaultListModel<>();
	/**
	 * The button for searching for a student by ID based on the ID provided in searchParameter
	 */
	private JButton btnSearchById;
	/**
	 * The button for searching for a student by last name based on the last name provided in searchParameter
	 */
	private JButton btnSearchByLastname;
	/**
	 * The list of students to be listed based on search results
	 */
	JList<StudentUser> studentList;
	
	/**
	 * Creates the panel for searching for students
	 */
	public StudentSearch() {
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		panel.setLayout(new FlowLayout(FlowLayout.LEFT, 10, 5));
		
		JLabel lblSearch = new JLabel("Search:");
		panel.add(lblSearch);
		
		searchParameter = new JTextField();
		searchParameter.setDocument(new JTextFieldLimit(40));
		panel.add(searchParameter);
		searchParameter.setColumns(10);
		
		btnSearchById = new JButton("Search by ID");
		btnSearchById.setActionCommand("ID");
		panel.add(btnSearchById);
		
		btnSearchByLastname = new JButton("Search by Last Name");
		btnSearchByLastname.setActionCommand("LN");
		panel.add(btnSearchByLastname);
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrollPane, BorderLayout.CENTER);
		
		studentList = new JList<>();
		studentList.setModel(model);
		studentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		studentList.setFont(new Font("Arial",Font.PLAIN,16));
		studentList.setFixedCellHeight(30);
		studentList.setFixedCellWidth(100);
		studentList.setBorder(new EmptyBorder(10,10, 10, 10));
		scrollPane.setViewportView(studentList);
		
		add(scrollPane, BorderLayout.CENTER);
	}
	
	/**
	 * Gets the text in searchParameter
	 * @return the text in serachParameter
	 */
	String getTextField() {
		return searchParameter.getText();
	}
	
	/**
	 * Gets the model
	 * @return the model
	 */
	DefaultListModel<StudentUser> getModel() {
		return model;
	}
	
	/**
	 * Sets the listener for btnSearchById
	 * @param l the desired listener
	 */
	void setSearchById(ActionListener l) {
		btnSearchById.addActionListener(l);
	}
	
	/**
	 * Sets the listener for btnSearchByLastname
	 * @param l the desired listener
	 */
	void setSearchByLastName(ActionListener l) {
		btnSearchByLastname.addActionListener(l);
	}
	
	/**
	 * Sets the listener for the student list
	 * @param l the desired listener
	 */
	void setListListener(ListSelectionListener l) {
		studentList.addListSelectionListener(l);
	}
}
