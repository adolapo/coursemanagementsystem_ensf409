package frontend;

import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextField;
import javax.swing.ListSelectionModel;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import data.ProfessorCourse;

/**
 * This view allows the professor to see all courses they have. They are able to browse their courses
 * and create new courses.
 * @author Babafemi Adeniran
 * @version 1.0
 * @since April 5, 2018
 *
 */
public class Home extends JPanel {
	/**
	 * The model for the list of courses
	 */
	private DefaultListModel <ProfessorCourse> model = new DefaultListModel<>();
	/**
	 * The list of courses
	 */
	JList<ProfessorCourse> list;
	/**
	 * The field where the name for a new course can be added
	 */
	private JTextField courseNameField;
	/**
	 * The button for adding a new course
	 */
	private JButton btnAddCourse;
	
	/**
	 * Creates the panel for the home screen based on the given list of courses
	 * @param courseList the courses to be listed
	 */
	public Home(ArrayList<ProfessorCourse> courseList) {
		setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		add(scrollPane, BorderLayout.CENTER);
		
		list = new JList<>();
		list.setModel(model);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setFont(new Font("Arial",Font.BOLD,16));
		list.setFixedCellHeight(25);
		list.setBorder(new EmptyBorder(10,10, 10, 10));
		scrollPane.setViewportView(list);
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.SOUTH);
		
		JLabel lblCourseName = new JLabel("New Course Name: ");
		panel.add(lblCourseName);
		
		courseNameField = new JTextField();
		courseNameField.setDocument(new JTextFieldLimit(40));
		panel.add(courseNameField);
		courseNameField.setColumns(10);
		
		btnAddCourse = new JButton("Add Course");
		panel.add(btnAddCourse);
		
		model.clear();
	      for(ProfessorCourse e : courseList){
	        model.addElement(e);
	     }
	}
	
	/**
	 * Gets the model
	 * @return the model
	 */
	DefaultListModel<ProfessorCourse> getModel() {
		return model;
	}
	
	/**
	 * Gets the text in courseNameField
	 * @return the courseNameField text
	 */
	String getCourseNameField() {
		return courseNameField.getText();
	}
	
	/**
	 * Sets the listener for list
	 * @param l the desired listener
	 */
	void setListListener(ListSelectionListener l) {
		list.addListSelectionListener(l);
	}
	
	/**
	 * Sets the listener for btnAddCourse
	 * @param l the desired listener
	 */
	void setAddNewCourseListener(ActionListener l) {
		btnAddCourse.addActionListener(l);
	}
}
