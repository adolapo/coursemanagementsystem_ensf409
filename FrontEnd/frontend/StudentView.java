package frontend;

import java.awt.BorderLayout;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComponent;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import data.Course;

/**
 * Implements the view for the student GUI for the course management system.
 * @author Babafemi Adeniran
 * @version 1.0
 * @since April 8, 2018
 */
public class StudentView extends JFrame {
	/**
	 * The main content view in the GUI
	 */
	private JPanel contentPane;
	/**
	 * Contains what component is occupying contentPane
	 */
	private JComponent currentContent;
	
	/**
	 * The name of the logged in user
	 */
	private JLabel lblName;
	/**
	 * The ID of the logged in user
	 */
	private JLabel lbID;
	/**
	 * The location the user is currently in in the GUI
	 */
	private JLabel lblLocationLabel;
	
	/**
	 * The button to go to the home view
	 */
	private JButton btnHome;
	
	/**
	 * The home view
	 */
	private StudentHome home; 
	/**
	 * The course view
	 */
	StudentCourseView scv; 
	
	/**
	 * Creates the frame for the view of the studnet GUI.
	 * @param home the starting home view
	 */
	public StudentView(StudentHome home) {
		this.home = home;
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 923, 575);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.NORTH);
		panel_1.setLayout(new BoxLayout(panel_1, BoxLayout.PAGE_AXIS));
		
		JPanel panel = new JPanel();
		panel_1.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.PAGE_AXIS));
		
		JPanel panel_5 = new JPanel();
		FlowLayout fl_panel_5 = (FlowLayout) panel_5.getLayout();
		fl_panel_5.setAlignment(FlowLayout.LEFT);
		panel.add(panel_5);
		
		JLabel lblName_1 = new JLabel("Name: ");
		lblName_1.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		panel_5.add(lblName_1);
		
		lblName = new JLabel("John Smith");
		panel_5.add(lblName);
		lblName.setFont(new Font("Lucida Grande", Font.ITALIC, 14));
		lblName.setHorizontalAlignment(SwingConstants.LEFT);
		
		JPanel panel_6 = new JPanel();
		FlowLayout fl_panel_6 = (FlowLayout) panel_6.getLayout();
		fl_panel_6.setAlignment(FlowLayout.LEFT);
		panel.add(panel_6);
		
		JLabel lblId = new JLabel("ID: ");
		lblId.setFont(new Font("Lucida Grande", Font.BOLD, 13));
		panel_6.add(lblId);
		
		lbID = new JLabel("12345678");
		lbID.setFont(new Font("Lucida Grande", Font.ITALIC, 14));
		panel_6.add(lbID);
		
		JPanel panel_2 = new JPanel();
		panel_1.add(panel_2);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.PAGE_AXIS));
		
		JPanel panel_3 = new JPanel();
		FlowLayout fl_panel_3 = (FlowLayout) panel_3.getLayout();
		panel_2.add(panel_3);
		
		lblLocationLabel = new JLabel("Home: List of Courses");
		lblLocationLabel.setFont(new Font("Arial",Font.BOLD,18));
		panel_3.add(lblLocationLabel);
		lblLocationLabel.setHorizontalAlignment(SwingConstants.CENTER);
		
		JPanel panel_4 = new JPanel();
		panel_2.add(panel_4);
		btnHome = new JButton("Home");
		btnHome.setFont(new Font("Arial",Font.PLAIN,18));
		panel_4.add(btnHome);
		btnHome.setAlignmentX(Component.RIGHT_ALIGNMENT);
		
		JButton btnLogout = new JButton("Logout");
		btnLogout.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int result = JOptionPane.showConfirmDialog(null, "Are you sure you want to logout?", "Logout Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null);
				if(result == JOptionPane.YES_OPTION) {
					System.exit(0);
				}
				else {
					return;
				}
			}
		});
		currentContent = home;
		getContentPane().add("Center", currentContent);
		contentPane.add(btnLogout, BorderLayout.SOUTH);
		setResizable(false);
	}
	
	/**
	 * Swaps the currentContent to be the home view
	 */
	void launchHome() {
		swapCurrentContent(home);
		lblLocationLabel.setText("Home: List of Courses");
	}
	
	/**
	 * Sets the listener for home button
	 * @param l the desired listener
	 */
	void setListeners(ActionListener l) {
		btnHome.addActionListener(l);
	}
	
	/**
	 * Sets the text in lblName
	 * @param lblName the lblName to set
	 */
	void setLblName(String lblName) {
		this.lblName.setText(lblName);;
	}

	/**
	 * Sets the text in lbID
	 * @param lbID the lbID to set
	 */
	void setLbID(String lbID) {
		this.lbID.setText(lbID);;
	}
	
	/**
	 * Launches the course view based on the given parameters.
	 * Listselection listener included to set up default view of course view
	 * @param c the course that the view will get information from
	 * @param l the desired listener
	 */
	void launchCourseView(Course c, ListSelectionListener l) {
		scv = new StudentCourseView(c, l);
		swapCurrentContent(scv);
		lblLocationLabel.setText(c.getCourseName());
	}
	
	/**
	 * Sets up sending an email view
	 * @param l the action listener for sending an email
	 */
	void email(ActionListener l) {
		scv.sendEmail(l);
		lblLocationLabel.setText(scv.course.getCourseName() + ": Email");
		getRootPane().validate();
		getRootPane().repaint();
	}
	
	/**
	 * Swaps the currentContent with given component
	 * @param c the component to be placed in currentContent
	 */
	void swapCurrentContent(JComponent c) {
		getContentPane().remove(currentContent);
		currentContent = c;
		getContentPane().add("Center", currentContent);
		getRootPane().validate();
		getRootPane().repaint();
	}
	
	/**
	 * Displays an error message dialog
	 * @param errorMessage the message to be displayed
	 */
	void displayErrorMessage (String errorMessage)
	{
		JOptionPane.showMessageDialog(this, errorMessage, "ERROR", JOptionPane.ERROR_MESSAGE);
	}
	
	/**
	 * Displays a normal message dialog
	 * @param message the message to be the displayed
	 * @param title the title to be displayed
	 */
	void displayMessage(String message, String title) {
		JOptionPane.showMessageDialog(this, message, title, JOptionPane.PLAIN_MESSAGE);
	}
	
	/**
	 * Sets the listeners for the home view
	 * @param l the listener for selecting courses
	 */
	void setHomeListeners(ListSelectionListener l){
		home.setListListener(l);
	}
	
	/**
	 * Sets the listeners for the course view
	 * @param l listener for sendEmail button
	 * @param l1 listener for assignments button
	 */
	void setCourseViewListeners(ActionListener l, ActionListener l1){	
		scv.setSendEmailListener(l); 
		scv.setAssignemntsListener(l1);
	}
	
	/**
	 * Gets the view's home
	 * @return the home
	 */
	StudentHome getHome() {
		return home;
	}
	
	/**
	 * Sets home to desired home
	 * @param home the home to set
	 */
	void setHome(StudentHome h) {
		home = h;
	}
}
