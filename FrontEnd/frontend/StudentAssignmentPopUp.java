package frontend;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

/**
 * A view for allowing a student to view an assignment, upload a submission, and see their grade
 * @author Babafemi Adeniran
 * @version 1.0
 * @since April 8, 2018
 *
 */
public class StudentAssignmentPopUp extends JDialog {
	/**
	 * The panel that contains the input fields, labels, and buttons
	 */
	private final JPanel contentPanel = new JPanel();
	/**
	 * The grade field
	 */
	private JTextField gradeField;
	/**
	 * The button for uploading a submission
	 */
	private JButton btnUpload;
	/**
	 * The button for downloading an assignment
	 */
	private JButton btnDownload;
	
	/**
	 * Creates an assignment pop up for a student based on given parameters
	 * @param grade the grade for their submission
	 * @param com the comment on their submission
	 */
	public StudentAssignmentPopUp(int grade, String com) {
		setBounds(100, 100, 508, 239);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BoxLayout(contentPanel, BoxLayout.PAGE_AXIS));
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			panel.setLayout(new FlowLayout(FlowLayout.CENTER, 5, 5));
			{
				btnDownload = new JButton("View Assignment");
				btnDownload.setBackground(Color.WHITE);
				btnDownload.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
				panel.add(btnDownload);
			}
			{
				btnUpload = new JButton("Upload Submisson");
				btnUpload.setFont(new Font("Lucida Grande", Font.PLAIN, 16));
				panel.add(btnUpload);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			panel.setLayout(new BorderLayout(5, 5));
			{
				JLabel lblComments = new JLabel("Comments: ");
				panel.add(lblComments, BorderLayout.NORTH);
			}
			{
				JTextArea comment = new JTextArea();
				comment.setText(com);
				comment.setEditable(false);
				panel.add(comment, BorderLayout.CENTER);
			}
		}
		{
			JPanel panel = new JPanel();
			contentPanel.add(panel);
			{
				JLabel lblGrade = new JLabel("Grade: ");
				lblGrade.setFont(new Font("Lucida Grande", Font.PLAIN, 15));
				panel.add(lblGrade);
			}
			{
				gradeField = new JTextField();
				gradeField.setEditable(false);
				if(grade == -1)
					gradeField.setText("---");
				else {
					gradeField.setText(String.valueOf(grade));
					btnUpload.setEnabled(false);
				}
				panel.add(gradeField);
				gradeField.setColumns(3);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Done");
				okButton.setActionCommand("OK");
				okButton.addActionListener(new ActionListener() {
					@Override
					public void actionPerformed(ActionEvent e) {
						dispose();
					}	
				});
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
		}
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}
	
	/**
	 * Sets the action listener for btnUpload
	 * @param l the desired listener
	 */
	void setUploadListener(ActionListener l) {
		btnUpload.addActionListener(l);
	}
	
	/**
	 * Sets the aciton listener for btnDownload
	 * @param l the desire listener
	 */
	void setDownloadListener(ActionListener l) {
		btnDownload.addActionListener(l);
	}
}
