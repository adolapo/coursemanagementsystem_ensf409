package frontend;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;

import data.Submission;
/**
 * This dialog box allows the professor to see, comment on, and grade a specific submission for an assignment.
 * @author Babafemi Adeniran
 * @version 1.0
 * @since April 5, 2018
 */
public class SubmissionPopUp extends JDialog {
	/**
	 * The panel that contains the input fields, labels, and buttons
	 */
	private final JPanel contentPanel = new JPanel();
	/**
	 * The grade desired
	 */
	private JTextField grade;
	/**
	 * The comment desired
	 */
	private JTextArea comment;
	/**
	 * The button for downloading and viewing a submission
	 */
	private JButton btnViewSubmission;
	/**
	 * The button for finishing grading, commenting, and/or viewing the submission
	 */
	private JButton doneButton;
	/**
	 * The submission that relates to the pop up
	 */
	Submission s;
	
	/**
	 * Creates a submission pop up for a professor based on the given parameters
	 * @param s the submission desired
	 */
	public SubmissionPopUp(Submission s) {
		this.s = s;
		setBounds(100, 100, 450, 203);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new BorderLayout(0, 0));
		{
			btnViewSubmission = new JButton("View Submission");
			btnViewSubmission.setEnabled(true);
			contentPanel.add(btnViewSubmission, BorderLayout.NORTH);
		}
		{
			JPanel commentPanel = new JPanel();
			contentPanel.add(commentPanel, BorderLayout.CENTER);
			commentPanel.setLayout(new BorderLayout(5, 5));
			{
				JLabel lblComment = new JLabel("Comment:");
				commentPanel.add(lblComment, BorderLayout.NORTH);
			}
			{
				comment = new JTextArea();
				comment.setLineWrap(true);
				comment.setDocument(new JTextFieldLimit(140));
				if(s.getComment() != null)
					comment.setText(s.getComment());
				commentPanel.add(comment, BorderLayout.CENTER);
			}
		}
		{
			JPanel gradePanel = new JPanel();
			contentPanel.add(gradePanel, BorderLayout.SOUTH);
			{
				JLabel lblGrade = new JLabel("Grade: ");
				gradePanel.add(lblGrade);
			}
			{
				// Check if a grade has been entered or not
				grade = new JTextField();
				grade.setColumns(3);
				grade.setDocument(new JTextFieldLimit(3));
				if(s.getAssignmentGrade() == -1)
					grade.setText("---");
				else {
					grade.setText(String.valueOf(s.getAssignmentGrade()));
				}
				
				gradePanel.add(grade);
			}
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				doneButton = new JButton("Done");
				doneButton.setActionCommand("OK");
				buttonPane.add(doneButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.setActionCommand("Cancel");
				cancelButton.addActionListener(new ActionListener() {

					@Override
					public void actionPerformed(ActionEvent e) {
						dispose();
					}	
				});
				buttonPane.add(cancelButton);
			}
		}
		setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
	}
	
	/**
	 * @return the grade
	 */
	String getGrade() {
		return grade.getText();
	}

	/**
	 * @return the comment
	 */
	String getComment() {
		return comment.getText();
	}
	
	/**
	 * Sets the listener for doneButton
	 * @param l the listener desired
	 */
	void setDoneButtonListener(ActionListener l) {
		doneButton.addActionListener(l);
	}
	
	/**
	 * Sets the listener for btnViewSubmission
	 * @param l the listener desired
	 */
	void setViewSubmissionListener(ActionListener l) {
		btnViewSubmission.addActionListener(l);
	}
}
