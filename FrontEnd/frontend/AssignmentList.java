package frontend;

import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.DefaultListModel;
import javax.swing.JList;
import javax.swing.ScrollPaneConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import data.CourseAssignment;

import java.awt.BorderLayout;
import java.awt.Font;
import java.util.ArrayList;
import javax.swing.ListSelectionModel;

/**
 * Lists the assignments a professor has uploaded in a course.
 * @author Babafemi Adeniran
 * @since April 5, 2018
 * @version 1.0
 */
public class AssignmentList extends JPanel {
	/**
	 * The list of assignments
	 */
	JList <CourseAssignment>assignmentList;
	/**
	 * The model for the assignments 
	 */
	DefaultListModel <CourseAssignment> model = new DefaultListModel<>();
	
	/**
	 * Sets the listener for assignmentList
	 * @param l the listener desired
	 */
	void setListListener(ListSelectionListener l) {
		assignmentList.addListSelectionListener(l);
	}
	
	/**
	 * Creates the panel and sets up the model to have courses based on the given parameter
	 * @param ca the courses desired
	 */
	public AssignmentList(ArrayList<CourseAssignment> ca) {
		setLayout(new BorderLayout(0, 0));
		assignmentList = new JList<>();
		assignmentList.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		assignmentList.setModel(model);
		assignmentList.setFont(new Font("Arial",Font.PLAIN,16));
		assignmentList.setFixedCellHeight(30);
		assignmentList.setFixedCellWidth(100);
		assignmentList.setBorder(new EmptyBorder(10,10, 10, 10));
		for(CourseAssignment e: ca) {
			model.addElement(e);
		}
		JScrollPane scrollPane = new JScrollPane();
		scrollPane.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_NEVER);
		scrollPane.setViewportView(assignmentList);
		add(scrollPane);
	}
}
