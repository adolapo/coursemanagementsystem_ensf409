package frontend;

import javax.swing.JPanel;
import java.awt.BorderLayout;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.util.ArrayList;

import javax.swing.JLabel;
import javax.swing.DefaultListModel;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ListSelectionListener;

import data.*;

import javax.swing.JList;
import javax.swing.ListSelectionModel;

/**
 * Shows details of a certain assignment. The user will be able to activate/deactivate the assingment and view any submissions
 * @author Babafemi Adeniran
 * @since April 5, 2018
 * @version 1.0
 */
public class AssignmentView extends JPanel {
	/**
	 * Activates/Deactivates the assignment
	 */
	JButton btnActivateDeactivate;
	/**
	 * The assignment the view relates to
	 */
	CourseAssignment c;
	/**
	 * The list of submissions related to the assignment
	 */
	ArrayList<Submission> submissionList;
	/**
	 * The model relating to submisisonLost
	 */
	private DefaultListModel <Submission> model = new DefaultListModel<>();
	/**
	 * The list view for submissions
	 */
	JList<Submission> list;
	/**
	 * The pop up that will show for a submission
	 */
	SubmissionPopUp spu;
	
	/**
	 * Creates the panel that holds all components and components are filled based on parameters.
	 * @param ca the course assignment desired
	 * @param s the list of submissions desired
	 */
	public AssignmentView(CourseAssignment ca, ArrayList<Submission> s) {
		c = ca;
		submissionList = s;
		setLayout(new BorderLayout(0, 0));
		
		JPanel panel = new JPanel();
		add(panel, BorderLayout.NORTH);
		
		JLabel lblAssignmentName = new JLabel(c.getTitle());
		panel.add(lblAssignmentName);
		
		JPanel panel_1 = new JPanel();
		add(panel_1, BorderLayout.SOUTH);
		btnActivateDeactivate = new JButton("Null");
		if(c.isActive() == 1) {
			btnActivateDeactivate.setText("Deactivate Assignment");
			btnActivateDeactivate.setActionCommand("D");
		}
		else {
			btnActivateDeactivate = new JButton("Activate Assignment");
			btnActivateDeactivate.setActionCommand("A");
		}
		panel_1.add(btnActivateDeactivate);
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new EmptyBorder(5, 5, 5, 5));
		add(panel_2, BorderLayout.CENTER);
		panel_2.setLayout(new BorderLayout(0, 0));
		
		JScrollPane scrollPane = new JScrollPane();
		panel_2.add(scrollPane);
		
		list = new JList<>();
		list.setModel(model);
		list.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		list.setFont(new Font("Arial",Font.PLAIN,16));
		list.setFixedCellHeight(30);
		list.setFixedCellWidth(100);
		list.setBorder(new EmptyBorder(10,10, 10, 10));
		model.clear();
	      for(Submission e : submissionList){
	        model.addElement(e);
	     }
		scrollPane.setViewportView(list);
		
		JLabel lblSubmissions = new JLabel("Submissions:");
		panel_2.add(lblSubmissions, BorderLayout.NORTH);
	}
	
	/**
	 * Brings the submisison pop up so that the professor can view the submission, grade it, and leave comments.
	 * @param s the submission desired
	 * @param d the action listener for done button for spu
	 * @param vs the action listener desired for view submission button for spu
	 */
	void viewSubmission(Submission s, ActionListener d, ActionListener vs) {
		spu = new SubmissionPopUp(s);
		setSubmissionListeners(d, vs);
		spu.setVisible(true);
	}
	
	/**
	 * Sets the listener for list
	 * @param l the listener desired
	 */
	void setListListener(ListSelectionListener l) {
		list.addListSelectionListener(l);
	}
	
	/**
	 * Sets the listener for btnActivateDeactivae
	 * @param l the listener desired
	 */
	void setActivateDeactivate(ActionListener l) {
		btnActivateDeactivate.addActionListener(l);
	}
	
	/**
	 * Sets the listeners for spu
	 * @param d the listener desired for done button
	 * @param vs the lister desired for view submission
	 */
	void setSubmissionListeners(ActionListener d, ActionListener vs){
		spu.setDoneButtonListener(d);
		spu.setViewSubmissionListener(vs);
	}
}
