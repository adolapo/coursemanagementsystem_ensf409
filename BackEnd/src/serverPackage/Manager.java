package serverPackage;
import data.*;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
/**
 * Program to manage Server/Client communication and Client servicing
 * @author Abdulkareem Dolapo
 * @since  April 3rd, 2018
 * @version 1.0
 */
class Manager implements Runnable, data.Requestcodes{
	/**
	 * Client object output stream of client
	 */
	private ObjectOutputStream outputStream;
	/**
	 * Object input stream of client
	 */
	private ObjectInputStream inputStream;
	/**
	 * DatabaseHelper to deal with retrieving and sending information to database
	 */
	private DataBaseHelper dbWorker;
	
	/**
	 * Default constructor
	 * @param in the input stream
	 * @param out the output stream
	 */
	public Manager(ObjectInputStream in, ObjectOutputStream out) {
		inputStream  =  in;
		outputStream = out;
		dbWorker = new DataBaseHelper(true);
	}
	
	/**
	 * Method that handles Server/Client communication
	 */
	public void run() {
		try{
			while(true) {
				Message receivedMessage = (Message)inputStream.readObject();
				Message responseMessage = processMessageRequest(receivedMessage);
				outputStream.writeObject(responseMessage);
			}
		}
		catch(ClassNotFoundException e) {
			e.printStackTrace();
		}
		catch(IOException e) {
			System.err.println("Server error while trying to read from client");
			e.printStackTrace();
		}
	}
	
	/**
	 * Method that decodes message and then process by taking appropriate action
	 * @param message
	 * @return
	 */
	private Message processMessageRequest(Message message) {
		int requestCode = message.getRequestCode();
		switch(requestCode) {
		case ENROLLMENT:
			return dbWorker.addNewEnrollment((Enrollment)message.getData());
		case STUDENT_USER:
			int studentId = ((User)message.getData()).getUserID();
			return dbWorker.constructStudentUser(studentId);
		case SUBMISSION:
			Submission submissionToAdd = (Submission)message.getData();
			return dbWorker.addNewSubmission(submissionToAdd);
		case PROFESSOR_COURSE:
			int profId = ((User)message.getData()).getUserID();
			return dbWorker.constructProfessorCourses(profId);
		case ASSIGNMENT:
			return dbWorker.addNewAssignments((CourseAssignment)message.getData());
		case EMAIL:
			System.out.println("Sending email");
			Email em = (Email)message.getData();
			User usr = em.getSender();
			String sender = dbWorker.getEmailAddress(usr.getUserID());
			System.out.println(sender);
			if(sender == null) {
				System.err.println("No email address to send");
				return new Message(0, new Email(null, null, null, 0));
			}
			ArrayList<String> receivers = new ArrayList<>();
			if(usr.getType().equals("S")) {
				Message m = dbWorker.constructCourseByCourseID(em.getCourseID());
				Course c = (Course)m.getData();
				String receiver = dbWorker.getEmailAddress(c.getProfId());
				receivers.add(receiver);
			}
			else {
				receivers = dbWorker.getStudentsAddress(em.getCourseID());
			}
			for(String e : receivers)
				System.out.println(e);
			EmailHelper eHelp = new EmailHelper();
			if(eHelp.sendEmail(sender, em.getContent(), em.getSubject(), receivers))
				return new Message(0, null);
			else
				return new Message(0, new Email(null, null, null, 0));
		case LOGIN:
			Login login = (Login)message.getData();
			return dbWorker.verifyLogin(login);
		case SEARCH_STUDENT_ID:
			int searchStudentId = ((StudentUser)message.getData()).getUserID();
			return dbWorker.constructStudentUser(searchStudentId);
		case SEARCH_STUDENT_LN:
			String lastName = ((StudentUser)message.getData()).getLastName();
			return dbWorker.searchStudentUserByLastName(lastName);
		case UNENROLLMENT:
			Enrollment doomedEnrollment = (Enrollment)message.getData();
			return dbWorker.deleteFromEnrollment(doomedEnrollment);
		case UPDATE_ASSIGNMENT:
			CourseAssignment toUpdate = (CourseAssignment)message.getData();
			return dbWorker.modifyAssignmentTable(toUpdate);
		case GET_SUBMISSION:
			int assignId = ((Submission)message.getData()).getAssignmentID();
			return dbWorker.searchSubmissionTable(assignId);
		case UPDATE_SUBMISSION:
			Submission update = (Submission)message.getData();
			return dbWorker.modifySubmissionTable(update);
		case UPDATE_COURSE:
			Course courseUpdate = (Course)message.getData();
			return dbWorker.modifyCourseTable(courseUpdate);
		case ADD_COURSE:
			Course courseToAdd = (Course)message.getData();
			return dbWorker.addNewCourses(courseToAdd);
		case VERIFY_SUBMISSION:
			Submission submissionToCheck = (Submission)message.getData();
			return dbWorker.searchSubmissionTableByAssignIdAndStudId(submissionToCheck.getAssignmentID(),
																	submissionToCheck.getUserID());
		default:
			return null;
		}
		
	}
	
	/**
	 * Sets the database helper
	 * @param helper the DataBaseHelper to set
	 */
	public void setDatabaseHelper(DataBaseHelper helper) {
		dbWorker = helper;
	}
}
