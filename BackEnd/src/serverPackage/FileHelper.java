package serverPackage;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * Program that handles file saving and retrieval
 * @author Abdulkareem Dolapo
 * @since April 5, 2018
 */
class FileHelper {
	/**
	 * Path to storage directory
	 */
	private final String STORAGE_DIRECTORY = 
			"/Users/babaadeniran/Desktop/School_Docs/ENSF 409/Final Project/coursemanagementsystem_ensf409/STORAGE";
	/**
	 * File Object required for file activities
	 */
	private File newFile;
	
	/**
	 * Default constructor
	 */
	public FileHelper() {
		//Does nothing
	}
	
	/**
	 * Builds a path to the file object given the file name
	 * @param fileName
	 * @return
	 */
	public String buildPath(String fileName) {
		String path = STORAGE_DIRECTORY  + fileName;
		return path;
	}
	
	/**
	 * Stores a file in STORAGE on the server
	 * @param content content of file to store
	 * @param path to  the file
	 */
	public void storeFile(byte[] content, String path) {
		newFile = new File(path);
		try{
			if(! newFile.exists())
				newFile.createNewFile();
			FileOutputStream writer = new FileOutputStream(newFile);
			BufferedOutputStream bos = new BufferedOutputStream(writer);
			bos.write(content);
			bos.close();
			writer.close();
		} 
		catch(IOException e){
				e.printStackTrace();
			}
	}
	
	/**
	 * Extracts contents of file into a byte array
	 * @param path the path to the file 
	 * @return byte array containing file contents
	 */
	public byte[] retrieveFileContents(String path) {
		File fileToRetrieve = new File(path);
		long length = fileToRetrieve.length();
		byte[] content = new byte[(int) length]; // Converting Long to Int
		try {
			FileInputStream fis = new FileInputStream(fileToRetrieve);
			BufferedInputStream bos = new BufferedInputStream(fis);
			bos.read(content, 0, (int)length);
			bos.close();
			fis.close();		
		} 
		catch (FileNotFoundException e) {
			e.printStackTrace();
		} 
		catch(IOException e){
			e.printStackTrace();
		}
		return content;
	}
}
